# mlogic-formalverf

**Description**

This is the repository corresponding to the formal verifications of 
constructive modal logics: S4 with necessity and full S4.
These formal verifications include all the statements characterizing:

(i) an axiomatic system inspired by Hakli and Negri’s system of derivations 
from assumptions for modal logic K [1], a Hilbert-style formalism designed to 
ensure the validity of the deduction theorem;

(ii)  natural deduction system, namely the judgmental reconstruction 
given by Pfenning and Davies [2];

(iii) together with the proof of their equivalence and complementary 
developments.

The proof approach taken throughout the work unveils the use of some 
alternative proof methods that allow for a smooth transition from the high-level 
mathematical proofs to their mechanized counterparts.

You will need the Coq proof assistant version 8.6 or higher 
(avaliable at https://coq.inria.fr/)

To intereact with the files, a Makefile is provided.


**Authors** 

Lourdes del Carmen González-Huesca

Favio E. Miranda-Perea

P. Selene Linares-Arévalo.

Departamento de Matemáticas, Facultad de Ciencias, UNAM. México.


**S4 with necessity**

The development in S4 corresponds to the intuitionistic and necessity fragment of S4. 
The CoqProject is named **MLS4**.
Axiomatic and dual systems for constructive necessity, a formally verified equivalence
There is a journal article reporting this work: 

https://www.tandfonline.com/doi/full/10.1080/11663081.2019.1647653

**Constructive S4**

The development in CS4 corresponds to the constructive S4.
The CoqProject is named **MLS4D**.

A conference paper reporting this work is https://doi.org/10.1016/j.entcs.2020.02.005

**Funding**

This work was supported by DGAPA-UNAM posdoctoral fellowships; and UNAM-PAPIME under grant PE102117.


**Principal References**

[1] Hakli, R., & Negri, S. (2012). Does the deduction theorem fail for modal 
logic? Synthese, 187 (3), 849–867.

[2] Pfenning, F., & Davies, R. (2001). A judgmental reconstruction of modal 
logic. Mathematical Structures in Comp. Sci., 11 (4), 511–540.
