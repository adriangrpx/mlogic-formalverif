(** GS4 from 'On Interactive Proof-Search for ConstructiveModal Necessity' *)

Require Import Coq.Program.Equality.
Require Import Coq.Arith.EqNat.
Require Import Coq.Init.Nat.
Require Import ModalLogic.
Require Import Sequents.
Require Import Slist.
Require Import Context.
Require Import List.

Definition LSeq : Type := list Prop.

(* Fixpoint ninslist (n : nat) (G : slist LabelledHyp) : bool :=
  match G with
  | empty => false
  | G', (pair m B) => beq_nat n m || ninslist n G'
  end.
 *)
(* Compute (ninslist 0 (empty, (pair 0 (Varp 0)))).
Compute leb 0 9. *)

(* Fixpoint testindex (G : slist LabelledHyp) : bool :=
  match G with
  | empty => true
  | G', (pair n A) => match G' with
    | empty => beq_nat n 0
    | G'', (pair m B) => beq_nat (S m) n && testindex G'
    end
  end.

Compute testindex (empty, (0 -: (Varp 0)), (1 -: (Varp 1)), (2 -: (Varp 2))).

Definition LCtx : Type := {s : slist LabelledHyp | is_true (testindex s)}.
 *)

(* Fixpoint lctxIndicesAux (G : LCtx) (L : list nat): list nat :=
  match G with
   | empty => L
   | G', (pair n B) => lctxIndicesAux G' (n :: L)
  end.

Definition lctxIndices (G : LCtx) : list nat :=
  lctxIndicesAux G nil.

Definition diff (l1 l2 : list nat) : list nat :=
  List.filter (fun n => negb (List.existsb (Nat.eqb n) l2)) l1.

Definition minIndex (G : LCtx): nat :=
 let l  := seq 0 (slist_length G) in
 let G' := lctxIndices G in
 match diff l G' with
  | nil => slist_length G
  | n :: _ => n
 end. *)

(* Definition ctxx : Type := slist LabelledHyp. *)

Definition pred_safe (n:nat) : n<>O -> nat :=
match n with
| O => fun Hn => False_rect _ (Hn (eq_refl O))
| S n => fun _ => n
end.

Fixpoint not_used_index (h: LabelledHyp) (G: slist LabelledHyp) : Prop :=
  match G with
  | empty => True
  | G', h' => (fst h) <> (fst h') /\ not_used_index h G'
  end.

Compute not_used_index (pair 0 (Varp 0)) (empty, (pair 0 (Varp 0))).

Definition pred_safes : forall n, n<>O -> nat.
Proof.
intros n Hn. destruct n.
destruct Hn. reflexivity.
exact n.
Qed.

(* Definition lcx: forall h G, not_used_index h G -> LCtx.
Proof.
intros.



Print pred_safes. *)

Definition lctxd (G : slist LabelledHyp) (h : LabelledHyp): 
  not_used_index h G -> lctx :=
match G with
| empty => fun _ => empty, h
| G', (pair n B) => fun Hn => snoc Hn G h
end.

Inductive lctx : Type :=
 | emptyc : lctx
 | snocc : forall (G : slist LabelledHyp) (h : LabelledHyp),
    not_used_index h G -> lctx.

Check snocc (_ emptyc (0 -: (Varp	 0))) : lctx.

Notation " G ; h " := (snocc G h) (at level 20, h at next level).

Definition newHypo (G : lctx) (A : Formula) : lctx :=
  match G with
  | empty => snocc empty (0 -: A)
  | G', (pair n B) => G, (pair (S  ) B)
  end.

(** --------------- INFERENCE RULES --------------- *)
(** The distinction between valid and true hypotheses is expressed by 
    separate sets of hypotheses, ie two contexts as arguments for the
    hypothetical judments.
    This definition incorporates the possibility judgment in 
    dedicated rules with explicit constructors. *)
Inductive DC_Proof : LCtx -> LCtx -> Formula -> Prop :=

| dc_thyp : forall (D: LCtx) (G G': LCtx) (A: Formula),
             DC_Proof D (newHypo G A; G') A

| dc_vhyp : forall (D D': LCtx) (G: LCtx) (A: Formula),
             DC_Proof (newHypo D A; D') G A

| dc_impR : forall (D: LCtx) (G: LCtx) (A B: Formula),
             DC_Proof D (newHypo G A) B -> DC_Proof D G (A ==> B)

| dc_conjR : forall (D: LCtx) (G: LCtx) (A B: Formula),
             DC_Proof D G A ->
             DC_Proof D G B -> DC_Proof D G (A ∧ B)

| dc_disjR_L : forall (D: LCtx) (G: LCtx) (A B: Formula),
             DC_Proof D G A -> DC_Proof D G (A ∨ B)

| dc_disjR_R : forall (D: LCtx) (G: LCtx) (A B: Formula),
             DC_Proof D G B -> DC_Proof D G (A ∨ B)

| dc_boxR : forall (D: LCtx) (G: LCtx) (A: Formula),
             DC_Proof D empty A -> DC_Proof D G (# A)

| dc_diaR : forall (D: LCtx) (G: LCtx) (A: Formula),
            DC_Proof D G A -> DC_Proof D G ($ A)

| dc_disjL : forall (D: LCtx) (G G': LCtx) (A B C: Formula),
             DC_Proof D (newHypo G A; G') C ->
             DC_Proof D (newHypo G B; G') C ->
             DC_Proof D (newHypo G (A ∨ B); G') C

| dc_conjL : forall (D: LCtx) (G G': LCtx) (A B C : Formula),
             DC_Proof D ((newHypo (newHypo G A) B); G') C -> 
             DC_Proof D (newHypo G (A ∧ B); G') C

| dc_impL : forall (D: LCtx) (G G': LCtx) (A B: Formula),
            DC_Proof D (newHypo G (A ==> B); G') A ->
            DC_Proof D (newHypo G (A ==> B); G') B

| dc_boxL : forall (D: LCtx) (G G': LCtx) (A B : Formula),
            DC_Proof (newHypo D A) (G; G') B ->
            DC_Proof D (newHypo G (# A); G') B

| dc_diaL: forall (D: LCtx) (G G': LCtx) (A C :Formula),
            DC_Proof D (newHypo empty A) ($ C) ->
            DC_Proof D (newHypo G ($ A); G') ($ C)

| dc_disjLV : forall (D D': LCtx) (G: LCtx) (A B C: Formula),
            DC_Proof (newHypo D (A ∨ B); D') (newHypo G A) C ->
            DC_Proof (newHypo D (A ∨ B); D') (newHypo G B) C ->
            DC_Proof (newHypo D (A ∨ B); D') G C

| dc_conjLV : forall (D D': LCtx) (G: LCtx) (A B C: Formula),
            DC_Proof ((newHypo (newHypo D A) B); D') G C ->
            DC_Proof ((newHypo D (A ∧ B)); D') G C

| dc_impLV : forall (D D': LCtx) (G: LCtx) (A B: Formula),
             DC_Proof (newHypo D (A ==> B); D') G A ->
             DC_Proof (newHypo D (A ==> B); D') G B

| dc_boxLV : forall (D D': LCtx) (G: LCtx) (A B : Formula),
            DC_Proof (newHypo D A; D') G B ->
            DC_Proof (newHypo D (# A); D') G B

| dc_diaLV: forall (D D' : LCtx) (G : LCtx) (A C : Formula),
            DC_Proof (newHypo D ($ A); D') (newHypo empty A) ($ C) ->
            DC_Proof (newHypo D ($ A); D') G ($ C)

| dc_cut : forall (D D': LCtx) (G G': LCtx) (A B : Formula),
            DC_Proof D G A ->
            DC_Proof D' (newHypo G' A) B ->
            DC_Proof (D;D') (G;G') B

| dc_cutV : forall (D: LCtx) (G: LCtx) (A C : Formula),
            DC_Proof D empty A ->
            DC_Proof (newHypo D A) G C ->
            DC_Proof D G C.


Global Hint Constructors DC_Proof : GS4.

Notation "D ∥ G ⊢s A" := (DC_Proof D G A) (at level 30).

Definition solvableT (S : SeqT) : Prop :=
 match S with
  | pair D p => match p with
    | pair G A => DC_Proof D G A
    end
  end.

Fixpoint solvableLSeqT (S : LSeqT) : Prop :=
 match S with
   | nil => True
   | s :: S' => solvableT s /\ solvableLSeqT S'
 end.

Lemma LSeqTConcNil: forall (S : LSeqT),
 S ++ nil = S.
Proof.
intros.
intuition.
Qed.

Lemma solvableLSeqTConc: forall (S1 S : LSeqT),
  solvableLSeqT (S1 ++ S) <-> solvableLSeqT S1 /\ solvableLSeqT S.
Proof.
intros.
induction S1; simpl in *; intuition.
Qed.

Notation "[ x ; .. ; y ]" := (cons x .. (cons y nil) ..).

Inductive Tactics : LSeqT -> LSeqT -> Prop :=

  | tassumption: forall (D G G': LCtx) (A : Formula),
    Tactics [ D ∥ (newHypo G A; G') ⊢ A ]
            nil

  | tvassumption: forall (D D' G : LCtx) (A : Formula),
    Tactics [ (newHypo D A; D') ∥ G ⊢ A ]
            nil

  | tintro: forall (D G : LCtx) (A B : Formula),
    Tactics [ D ∥ G ⊢ (A ==> B) ]
            [ D ∥ (newHypo G A) ⊢ B ]

  | tsplit: forall (D G : LCtx) (A B : Formula),
    Tactics [ D ∥ G ⊢ (A ∧ B) ]
            ( [D ∥ G ⊢ A] ++ [D ∥ G ⊢ B] )

  | tleft: forall (D G : LCtx) (A B : Formula),
    Tactics [ D ∥ G ⊢(A ∨ B) ]
            [ D ∥ G ⊢ A ]

  | tright: forall (D G : LCtx) (A B : Formula),
    Tactics [ D ∥ G ⊢ (A ∨ B) ]
            [ D ∥ G ⊢ B ]

  | tneccesitation: forall (D G : LCtx) (A : Formula),
    Tactics [ D ∥ G ⊢ (# A) ]
            [ D ∥ empty ⊢ A ]

  | tdia : forall (D G : LCtx) (A : Formula),
    Tactics [ D ∥ G ⊢ ($ A) ]
            [ D ∥ G ⊢ A ]

  | tdestructdisj: forall (D G G' : LCtx) (A B C: Formula),
    Tactics [ D ∥ (newHypo G (A ∨ B); G') ⊢ C ]
            ( [ D ∥ (newHypo G A; G') ⊢ C]
           ++ [ D ∥ (newHypo G B; G') ⊢ C] )

  | tdestructconj: forall (D G G' : LCtx) (A B C: Formula),
    Tactics [ D ∥ (newHypo G (A ∧ B); G') ⊢ C ]
            [ D ∥ ((newHypo (newHypo G A) B); G') ⊢ C ]

  | tapply: forall (D G G' : LCtx) (A B : Formula),
    Tactics [ D ∥ (newHypo G (A ==> B); G') ⊢ B ]
            [ D ∥ (newHypo G (A ==> B); G') ⊢ A ]

  | tdestructbox: forall (D G G' : LCtx) (A B: Formula),
    Tactics [ D ∥ (newHypo G (# A); G') ⊢ B ]
            [ (newHypo D A) ∥ (G ; G') ⊢ B]

  | tdial: forall (D G G' : LCtx) (A C: Formula),
    Tactics [ D ∥ (newHypo G ($ A); G') ⊢ ($ C) ]
            [ D ∥ (newHypo empty A) ⊢ ($ C) ]

  | tvdestructdisj: forall (D D' G : LCtx) (A B C: Formula),
    Tactics [   (newHypo D (A ∨ B); D') ∥ G ⊢ C ]
            ( [ (newHypo D (A ∨ B); D') ∥ (newHypo G A) ⊢ C]
           ++ [ (newHypo D (A ∨ B); D') ∥ (newHypo G B) ⊢ C] )

  | tvdestructconj: forall (D D' G : LCtx) (A B C: Formula),
    Tactics [ (newHypo D (A ∧ B); D') ∥ G ⊢ C ]
            [ (newHypo (newHypo D A) B); D' ∥ G ⊢ C ]

  | tvapply: forall (D D' G : LCtx) (A B : Formula),
    Tactics [ (newHypo D (A ==> B); D') ∥ G ⊢ B ]
            [ (newHypo D (A ==> B); D') ∥ G ⊢ A ]

  | tvdestructbox: forall (D D' G : LCtx) (A B: Formula),
    Tactics [ (newHypo D (# A); D') ∥ G ⊢ B ]
            [ (newHypo D A; D') ∥ G ⊢ B ]

  | tdiaLV: forall (D D' G : LCtx) (A C: Formula),
    Tactics [ (newHypo D ($ A); D') ∥ G ⊢ ($ C) ]
            [ (newHypo D ($ A); D') ∥ (newHypo empty A) ⊢ ($ C) ]

  | tcut: forall (D D' G G' : LCtx) (A B: Formula),
    Tactics [ (D; D') ∥ (G; G') ⊢ B ]
            ( [ D' ∥ (newHypo G' A) ⊢ B ] ++ [ D ∥ G ⊢ A ] )

  | tvcut: forall (D G : LCtx) (A B: Formula),
    Tactics [ D ∥ G ⊢ B ]
            ( [ (newHypo D A) ∥ G ⊢ B] ++ [ D ∥ empty ⊢ A] )

  | tassert: forall (D G : LCtx) (A C: Formula),
    Tactics [ D ∥ G ⊢ C ]
            ( [ D ∥ G ⊢ A ] ++ [ D ∥ (newHypo G A) ⊢ C] )

  | tvassert: forall (D G : LCtx) (A C: Formula),
    Tactics [ D ∥ G ⊢ C ]
            ( [ D ∥ empty ⊢ A] ++ [ (newHypo D A) ∥ G ⊢ C] )

  | seq: forall (S1 S2 S : LSeqT),
    Tactics S1 S2 -> Tactics (S1 ++ S) (S2 ++ S).

Global Hint Constructors Tactics : GS4.

Notation "S ▷ S'" := (Tactics S S') (at level 30).

Inductive TacticsCls : LSeqT -> LSeqT -> Prop :=

  | tc1: forall (S S' : LSeqT),
    S ▷ S' -> TacticsCls S S'

  | tc2: forall (S S' S'' : LSeqT),
    S ▷ S' -> TacticsCls S' S'' -> TacticsCls S S''.

Notation "S ▷+ S'" := (TacticsCls S S') (at level 30).

(* Lemma 4.2 *)
Lemma SeqPlus:
  forall (S S1 S2 : LSeqT),
  S1 ▷+ S2 -> (S1 ++ S) ▷+ (S2 ++ S).
Proof.
intros.
dependent induction H.
+ apply tc1.
  apply seq.
  assumption.
+ eapply tc2.
  apply seq.
  exact H.
  assumption.
Qed.

Lemma ss:
 forall (D G: LCtx) (A : Formula) (x : LabelledHyp) ,
  DC_Proof D (G, x) A <-> DC_Proof D (newHypo G (snd x)) A.
Proof.
 intros.
split.
- admit.
- intros.
  dependent induction H.
  + admit.
  + unfold newHypo.
    apply dc_vhyp.
  + apply dc_impR.
    apply IHDC_Proof.
    simpl.
    induction G.
Admitted.

Lemma ss2:
 forall (D G G': LCtx) (A : Formula) (x : LabelledHyp) ,
  DC_Proof D ((G, x); G') A -> DC_Proof D ((newHypo G (snd x)); G') A.
Proof.
Admitted.


Lemma ctxContractionLeft :
  forall (D : LCtx) (G : LCtx) (A : Formula),
  DC_Proof (D; D) G A -> DC_Proof D G A.
Proof.
intro D.
induction D.
intros.
simpl in H.
exact H.
intros.

Admitted.

Theorem DeductionTh:
  forall (D G : LCtx) (A B : Formula),
  DC_Proof D (newHypo G A) B -> DC_Proof D G (A ==> B).
Proof.
intros.

Admitted.

Lemma inverseDT:
  forall (D G : LCtx) (A B : Formula), 
  DC_Proof D G (A ==> B) -> DC_Proof D (newHypo G A) B.
Proof.
intros.
assert (DC_Proof D (newHypo empty A) A).
change (newHypo empty A) with ((newHypo empty A); empty).
apply dc_thyp.
(* change (newHypo G A) with (G; (newHypo empty A)).
eapply MP.
exact H0.
exact H. *)
Admitted.

Corollary contraction_hyp:
  forall (D G : LCtx) (A B : Formula),
  DC_Proof D (newHypo (newHypo G A) A) B -> DC_Proof D (newHypo G A) B.
Proof.
intros.
apply inverseDT.
repeat apply DeductionTh in H.
(* assert(K:= Ax2 empty A B).
rewrite <- (ctx_empty_conc G).
eapply MP.
exact H.
assumption. *)
Admitted.

Lemma Ax2_dett: 
  forall (D G : LCtx) (A B : Formula),
  DC_Proof D G (A ==> A ==> B) -> DC_Proof D G (A ==> B).
Proof.
intros.
apply inverseDT in H.
apply inverseDT in H.
apply contraction_hyp in H.
apply DeductionTh in H.
assumption.
Qed.

Lemma Ax3_dett: 
  forall (D G : LCtx) (A B C: Formula),
  DC_Proof D G (A ==> B ==> C) -> DC_Proof D G (B ==> A ==> C).
Proof.
intros.
rewrite <- (ctx_empty_conc G).
(* eapply MP.
- exact H.
- apply Ax3. *)
Admitted.

Theorem deductionTh_genPremise: 
  forall (G' G D: LCtx) (A B: Formula),
  DC_Proof D (newHypo G A; G') B -> DC_Proof D (G; G') (A ==> B).
Proof.
intro.
induction G' ; auto ; intros.
+ simpl in H.
  apply dc_impR in H.
  intuition.
+ simpl  in H.
  apply ss in H.
  apply DeductionTh in H.
  apply IHG' in H.
  apply Ax3_dett in H.
  apply inverseDT in H.
  apply ss in H.
  intuition.
Admitted.

Lemma ctxContractionRight :
  forall (G : LCtx) (D : LCtx) (A : Formula),
  DC_Proof D (G; G) A -> DC_Proof D G A.
Proof.
intro G.
induction G.
- intros.
  exact H.
- intros.
  apply ss2 in H.
  apply deductionTh_genPremise in H.
  apply ss in H.
  apply DeductionTh in H.
  apply Ax2_dett in H.
  apply IHG in H.
  apply inverseDT in H.
  change (G, x) with ((G, x); empty).
  apply ss2.
  simpl.
  apply inverseDT.
  exact H.
Qed.

(* Lemma 4.4 *)
Lemma SeqSolv:
  forall (S1 S2 : LSeqT),
  S1 ▷ S2 -> solvableLSeqT S2 -> solvableLSeqT S1.
Proof.
intros.
dependent induction H.
1-19: simpl in *; split; intuition.
- eapply dc_cut.
  exact H0.
  exact H.
- simpl in *; split; intuition.
  eapply dc_cutV.
  exact H0.
  exact H.
- simpl in *. split; intuition.
  apply ctxContractionLeft.
  apply ctxContractionRight.
  eapply dc_cut.
  exact H.
  exact H0.
- simpl in *; split; intuition.
  eapply dc_cutV.
  exact H.
  exact H0.
- apply solvableLSeqTConc.
  apply solvableLSeqTConc in H0.
  intuition.
Qed.

(* Lemma 4.5 *)
Lemma SeqSolvPlus:
  forall (S1 S2 : LSeqT),
  S1 ▷+ S2 -> solvableLSeqT S2 -> solvableLSeqT S1.
Proof.
intros.
dependent induction H.
- apply SeqSolv in H.
  exact H.
  exact H0.
- apply SeqSolv in H.
  exact H.
  apply IHTacticsCls.
  exact H1.
Qed.

Lemma concatNil:
  forall (S1 S2 : LSeqT),
  S1 ▷+ nil -> S2 ▷+ nil -> (S1 ++ S2) ▷+ nil.
Proof.
intros.
dependent induction H.
- apply seq with (S := S2) in H.
  simpl in H.
  eapply tc2.
  exact H.
  exact H0.
- intuition.
  apply seq with (S := S2) in H.
  eapply tc2.
  exact H.
  exact H3.
Qed.

Lemma derivableSolvable:
  forall (D G : LCtx) (A : Formula),
  DC_Proof D G A <-> solvableLSeqT [D ∥ G ⊢ A].
Proof.
intros.
split.
- intros.
  unfold solvableLSeqT.
  intuition.
- intros.
  unfold solvableLSeqT in H.
  intuition.
Qed.

(* Theorem 4.6 *)
Theorem forwardBackwardEquivs:
  forall (S : SeqT),
  match S with
  | pair D (pair G A) => DC_Proof D G A <-> [S] ▷+ nil
  end.
Proof.
intros.
destruct S as [D].
destruct p as [G A].
split.
- intros.
  dependent induction H.
  + apply tc1.
    apply tassumption.
  + apply tc1.
    apply tvassumption.
  + eapply tc2.
    apply tintro.
    exact IHDC_Proof.
  + eapply tc2.
    apply tsplit.
    apply concatNil.
    exact IHDC_Proof1.
    exact IHDC_Proof2.
  + eapply tc2.
    apply tleft.
    exact IHDC_Proof.
  + eapply tc2.
    apply tright.
    exact IHDC_Proof.
  + eapply tc2.
    apply tneccesitation.
    exact IHDC_Proof.
  + eapply tc2.
    apply tdia.
    exact IHDC_Proof.
  + eapply tc2.
    apply tdestructdisj.
    apply concatNil.
    exact IHDC_Proof1.
    exact IHDC_Proof2.
  + eapply tc2.
    apply tdestructconj.
    exact IHDC_Proof.
  + eapply tc2.
    apply tapply.
    exact IHDC_Proof.
  + eapply tc2.
    eapply tdestructbox.
    exact IHDC_Proof.
  + eapply tc2.
    apply tdial.
    exact IHDC_Proof.
  + eapply tc2.
    apply tvdestructdisj.
    apply concatNil.
    exact IHDC_Proof1.
    exact IHDC_Proof2.
  + eapply tc2.
    apply tvdestructconj.
    exact IHDC_Proof.
  + eapply tc2.
    apply tvapply.
    exact IHDC_Proof.
  + eapply tc2.
    apply tvdestructbox.
    exact IHDC_Proof.
  + eapply tc2.
    apply tdiaLV.
    exact IHDC_Proof.
  + eapply tc2.
    apply tcut.
    apply concatNil.
    exact IHDC_Proof2.
    exact IHDC_Proof1.
  + eapply tc2.
    apply tvassert.
    apply concatNil.
    exact IHDC_Proof1.
    exact IHDC_Proof2.
- intros.
  apply SeqSolvPlus in H.
  unfold solvableLSeqT in H.
  intuition.
  simpl.
  intuition.
Qed.