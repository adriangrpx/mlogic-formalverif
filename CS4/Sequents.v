Require Import ModalLogic.
Require Import CS4Dia.
Require Import Context.
Require Import Slist.

Definition LabelledHyp : Type := (nat * Formula)%type.

Notation "n -: A " := (pair n A) (at level 20).

(* Definition LCtx : Type := slist LabelledHyp. *)

(* Definition SeqT : Type := (LCtx * (LCtx * Formula))%type. *)

(* Notation "D ∥ G ⊢ A" := (pair D (pair G A)) (at level 20).

(* Definition 3.2 *)
Definition LSeqT : Type := list SeqT. *)