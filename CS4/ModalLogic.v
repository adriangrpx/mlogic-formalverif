(**
 Modal logic 
 Language: propositional modal logic with implication, necessity and possibility operators.
 *)

Require Import Coq.Strings.String.
Require Import Arith.
Require Import Bool.

Open Scope nat_scope.

Set Implicit Arguments.


(** --------------- SYNTAX OF MODAL FORMULAE --------------- *)

Inductive Formula : Type :=
  | Varp : nat -> Formula
  | Impl : Formula -> Formula -> Formula
  | Box  : Formula -> Formula
  | Dia  : Formula -> Formula
  | And  : Formula -> Formula -> Formula
  | Or   : Formula -> Formula -> Formula.

Notation "x ==> y"  := (Impl x y) (at level 55, right associativity).
Notation "# x" := (Box x) (at level 54, right associativity).
Notation "$ x" := (Dia x) (at level 54, right associativity).
Notation "x ∧ y" := (And x y) (at level 53, right associativity).
Notation "x ∨ y" := (Or x y) (at level 53, right associativity).

(** Definition of equality between Formulas *)
Proposition eq_p_dec (A B:Formula): {A = B} + {A <> B}.
Proof.
intros.
decide equality.
apply eq_nat_dec.
Qed.

Global Hint Resolve eq_p_dec : formula.


(** Judgments to distinguish true and possible conclusions of hypothetical judgments *)
Inductive Judgm: Type :=
 | JTrue : Formula -> Judgm
 | JPoss : Formula -> Judgm.


(** Definition of equality between judgments *)
Proposition eq_j_dec (A B:Judgm): {A = B} + {A <> B}.
Proof.
intros.
decide equality; apply eq_p_dec.
Qed.

Global Hint Resolve eq_j_dec : formula.

