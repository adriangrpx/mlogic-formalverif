Require Import ModalLogic.
Require Import CS4Dia.
Require Import Context.
Require Import Slist.

Definition LabelledHyp : Type := (nat * Formula)%type.

Notation "n -: A " := (pair n A) (at level 20).

Definition LCtx : Type := slist LabelledHyp.

(* Definition 3.2 *)
Definition LSeq : Type := list Prop.

