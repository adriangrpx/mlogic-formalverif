Require Import ModalLogic.
Require Import Slist.

Definition ctx := slist Formula.

(** Definition of a boxed context *)
Fixpoint boxed (c: ctx) : ctx :=
  match c with
  | empty => empty
  | snoc G' b  => snoc (boxed G') (Box b)
  end.
  
Global Hint Unfold boxed : context.

(** Boxed context properties *)

Lemma boxed_conc:
forall (G G': ctx), boxed (G;G') = (boxed G);(boxed G').
Proof.
intros.
induction G'; auto.
simpl.
rewrite IHG'.
reflexivity.
Qed.

Global Hint Resolve boxed_conc : context.

Lemma elem_boxed: 
  forall (G: ctx) (A: Formula), elem A G -> elem (#A) (boxed G).
Proof.
intros.
induction G.
- intuition.
- destruct H.
+ subst. simpl. left. reflexivity.
+ simpl. right. apply IHG. assumption. 
Qed.

Global Hint Resolve elem_boxed : context.

(* 8 - junio - 2021 *)
Lemma boxed_snoc: 
  forall (G: ctx) (A: Formula), boxed G, (# A) = boxed (G, A).
Proof.
intros.
reflexivity.
Qed.

Global Hint Resolve boxed_snoc : context.

(* 8 - junio - 2021 *)