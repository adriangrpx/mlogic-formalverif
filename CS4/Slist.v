(** Slists *)

Require Import Coq.Program.Equality.
Require Import Lia.

Set Implicit Arguments.

(** Definition of Slist *)
Inductive slist (X: Type): Type :=
 | empty : slist X
 | snoc : slist X -> X -> slist X.

Notation " G , p " := (snoc G p) (at level 20, p at next level).

Arguments empty {X}.
Arguments snoc {X} _ _.

Fixpoint slist_length {X : Type} (l : slist X): nat :=
  match l with
   | empty => 0
   | l', x => S (slist_length l')
  end.

Global Hint Unfold slist_length : slist.

Fixpoint slist_hd {X: Type} (l: slist X) : option X :=
  match l with
  | empty => None
  | empty, x => Some x
  | l', x => slist_hd l'
  end.
  
Global Hint Unfold slist_hd : slist.

(* Fixpoint slist_tl {X: Type} (l: slist X) : slist X :=
  match l with
  | empty => empty
  | empty, x => empty
  | l', x => (slist_tl l'), x
  end.

Global Hint Unfold slist_tl : slist.

Compute slist_tl (empty, Varp 0, Varp 1, Varp 2).
Compute slist_hd (empty, Varp 0, Varp 1, Varp 2).

Fixpoint slist_nth {X : Type} (l : slist X) (n : nat)
                   : option X :=
  match l with
  | empty => None
  | G', x => match n with
               | 0 => slist_hd G'
               | S n' => slist_nth (slist_tl l) n'
               end
  end.

Notation " G ! n " := (slist_nth G n) (at level 20).

Global Hint Unfold slist_nth : slist.
 *)
(* Fixpoint slist_nth_aux {X : Type} (l : slist X) (n : nat)
                   : option X :=
  match l with
  | empty => None
  | G', x => match n with
               | 0 => Some x
               | S n' => slist_nth_aux G' n'
               end
  end.
  
Global Hint Unfold slist_nth_aux : slist.

Definition slist_nth {X : Type} (l : slist X) (n : nat) : option X :=
  match slist_length l - n with
  | 0 => None
  | other => slist_nth_aux l (slist_length l - n - 1)
  end.
 *)

Fixpoint slist_nth {X : Type} (l : slist X) (n : nat) : option X :=
  match l with
  | empty => None
  | G', x => match n with
               | 0 => Some x
               | S n' => slist_nth G' n'
               end
  end.

Notation " G ! n " := (slist_nth G n) (at level 20).

Global Hint Unfold slist_nth : slist.

(** Definition of equality between contexts *)
Proposition eq_ctx_dec {X: Type} (G G': slist X) 
  (deceqX: forall (x x': X), {x = x'} + {x <> x'}): {G = G'} + {G <> G'}.
Proof.
intros.
decide equality.
Defined.

Global Hint Resolve eq_ctx_dec : slist.

(** Decidability of empty context *)
Proposition eq_ctx_dec_empty {X: Type} (G: slist X): {G = empty} + {G <> empty}.
Proof.
intros.
destruct G.
- left. reflexivity.
- right. discriminate.
Defined.

Global Hint Resolve eq_ctx_dec_empty : slist.


(** --------------- CONTEXT OPERATIONS --------------- *)

(** Context concatenation *)
Fixpoint conc {X: Type} (G G': slist X) : slist X :=
  match G' with
  | empty => G
  | snoc D q => snoc (conc G D) q
  end.

Global Hint Unfold conc : slist.

Notation " G ; D " := (conc G D) (at level 20).

Proposition length_distr {X: Type} (G G' : slist X):
  slist_length (G;G') = slist_length G + slist_length G'.
Proof.
induction G'.
- simpl. 
  apply plus_n_O.
- simpl.
  rewrite IHG'.
  apply plus_n_Sm.
Qed.

Global Hint Resolve length_distr : slist.

(** Membership function *)
Fixpoint elem {X: Type} (a: X) (G: slist X) : Prop :=
  match G with
  | empty => False
  | G',b => b = a \/ elem a G'
  end.

Global Hint Unfold elem : slist.


(** Function that constructs a context with 
n occurrences of a given formula*)

Fixpoint replicate {X: Type} (A: X) (n: nat): slist X :=
  match n with
  | 0 => empty
  | S n => (replicate A n,A)
  end.

Global Hint Unfold replicate : slist.

(** --------------- CONTEXT PROPERTIES ---------------*)

(** ABOUT ELEM *)

Lemma elem_empty: 
  forall {X: Type} (A: X), ~ elem A empty.
Proof.
intros.
simpl.
unfold not.
trivial.
Qed.

Global Hint Resolve elem_empty : slist.


Lemma elem_ctxhead:
  forall {X: Type} (A: X) (G: slist X), elem A (G,A).
Proof.
intros.
simpl.
intuition.
Qed.

Global Hint Resolve elem_ctxhead : slist.


Lemma elem_ctxcons:
  forall {X: Type} (A B: X) (G: slist X), elem B G -> elem B (G,A).
Proof.
intros.
simpl.
right.
trivial.
Qed.

Global Hint Resolve elem_ctxcons : slist.


Lemma elem_mid:
  forall {X: Type} (A: X) (G G': slist X), elem A ((G,A);G').
Proof.
intros.
induction G'.
- simpl. left. reflexivity.
- assert(((G, A); (G', x)) = ((G, A); G'), x).
  + simpl. reflexivity.
  + rewrite H. apply elem_ctxcons. assumption.
Qed.

Global Hint Resolve elem_mid : slist.


Lemma elem_inv:
  forall {X: Type} (A B: X) (G: slist X), elem B (G,A) -> (A = B) \/ elem B G.
Proof.
intros.
inversion H.
- left. assumption.
- right. assumption.
Qed.

Global Hint Resolve elem_inv : slist.


Lemma elem_ctxsplit:
  forall {X: Type} (A: X) (G: slist X), elem A G -> exists G1, exists G2, G=(G1,A);G2.
Proof.
intros.
induction G.
- elim H.
- inversion H.
  + exists G.
    exists empty.
    rewrite H0.
    reflexivity.
  + apply IHG in H0. 
    destruct H0.
    destruct H0 in *.
    exists x0. exists (x1,x).
    simpl.
    rewrite H0.
    reflexivity.
Qed.

Global Hint Resolve elem_ctxsplit : slist.


Lemma elem_conc_split: 
  forall {X: Type} (A: X) (G G': slist X), elem A (G;G') -> elem A G \/ elem A G'.
Proof.
intros.
induction G'; simpl in H.
- left; intuition.
- destruct H.
  + right; intuition.
  + apply IHG' in H. 
    destruct H; intuition.
Qed.

Global Hint Resolve elem_conc_split : slist.


Lemma elem_conc_L: 
  forall {X: Type} (A: X) (G: slist X), elem A G -> forall (G': slist X), elem A (G;G').
Proof.
intros.
induction G'.
- simpl. trivial.
- case (elem_conc_split A G G' IHG'); intros; simpl; right; assumption.
Qed.

Global Hint Resolve elem_conc_L : slist.


Lemma elem_conc_R: 
  forall {X: Type} (A: X) (G G': slist X), elem A G' -> elem A (G;G').
Proof.
intros.
induction G'; intros.
- inversion H.
- simpl conc.
  inversion H; simpl.
  + left. exact H0.
  + right. apply IHG'. exact H0.
Qed.
 
Global Hint Resolve elem_conc_R : slist.

Lemma elemSplit: 
  forall {X: Type} (G: slist X) (A B: X), B <> A -> elem B G -> 
  forall (G0 G0': slist X), G ~= (G0,A);G0' -> elem B (G0;G0').
Proof.
intros.
rewrite H1 in H0.
assert (elem B (G0,A) \/ elem B G0').
- apply elem_conc_split. assumption.
- destruct H2.
  + assert (A=B \/ elem B G0).
    -- apply elem_inv. assumption.
    -- destruct H3.
       * intuition.
       * apply elem_conc_L. assumption.
  + apply elem_conc_R. assumption.
Qed.

Global Hint Resolve elemSplit : slist.

(** --------------- ABOUT SNOC and CONC --------------- *)

Proposition ctx_eq_snoc:
  forall {X: Type} (G G': slist X) (A: X), G = G' -> G,A = G',A.
Proof.
intros.
rewrite H.
reflexivity.
Qed.

Global Hint Resolve ctx_eq_snoc : slist.


Proposition ctx_eq_snoc_cancell:
  forall {X: Type} (G G': slist X) (A: X), G,A = G',A -> G = G'.
Proof.
intros.
inversion H.
reflexivity.
Qed.

Global Hint Resolve ctx_eq_snoc_cancell : slist.

Lemma ctx_eq_snoc_empty:
  forall {X: Type} (G : slist X) (A : X), (G,A) = ((G,A) ; empty).
Proof.
intros.
simpl.
reflexivity.
Qed.

Global Hint Resolve ctx_eq_snoc_empty : slist.

Lemma ctx_eq_conc_empty:
  forall {X: Type} (G G': slist X), (G;G') = empty -> G = empty /\ G'= empty.
Proof.
intros.
destruct G; destruct G'; (split; reflexivity)||discriminate H.
Qed.

Global Hint Resolve ctx_eq_conc_empty : slist.


Lemma ctx_empty_conc: 
  forall {X: Type} (G : slist X) , (empty;G) = G.
Proof.
intros.
induction G; simpl; intuition.
Qed.

Global Hint Resolve ctx_empty_conc : slist.


Lemma ctx_conc_empty: 
  forall {X: Type} (G : slist X), (G;empty) = G.
Proof.
intros.
reflexivity.
Qed.

Global Hint Resolve ctx_conc_empty : slist.


Lemma ctx_snoc_conc:
  forall {X: Type} (G G': slist X) (A B: X), (((G,A);G'),B) = ((G,A);(G',B)).
Proof.
intros.
simpl.
reflexivity.
Qed.

Global Hint Resolve ctx_snoc_conc : slist.


Lemma ctx_snoc_concbis :
  forall {X: Type} (G G': slist X) (A B: X), (((G,A),B);G')=((G,A);((empty,B);G')).
Proof.
intros.
induction G'.
- simpl. reflexivity.
- simpl. rewrite IHG'. reflexivity.
Qed.

Global Hint Resolve ctx_snoc_concbis : slist.

Lemma ctx_conc_conc:
  forall {X: Type} (G G' G'' : slist X), G; (G';G'') = (G;G');G''.
Proof.
intros.
induction G''; simpl.
- reflexivity.
- rewrite IHG''. reflexivity.
Qed.

Global Hint Resolve ctx_conc_conc : slist.


Lemma ctx_nempty_split:
  forall {X: Type} (G: slist X), G <> empty -> exists (G1 G2: slist X), G = G1;G2.
Proof.
intros.
induction G.
- exists empty. exists empty. simpl. reflexivity.
- case_eq G; intros.
  + exists empty. exists (empty,x). simpl. reflexivity.
  + rewrite H0 in IHG.
    destruct IHG.
    -- intuition. 
     apply H. discriminate H1.
    -- destruct H1.
       exists x1.
       exists (x2,x).
       rewrite H1.
       simpl.
       reflexivity.
Qed.

Global Hint Resolve ctx_nempty_split : slist.


Lemma ctx_inv_nonempty: 
  forall {X: Type} (G: slist X), G <> empty -> exists (G': slist X) (A: X), G = G',A.
Proof.
intros.
induction G.
intuition.
exists G.
exists x.
reflexivity.
Qed.

Global Hint Resolve ctx_inv_nonempty : slist.

Lemma ctx_decomposition:
  forall {X: Type} (G D P: slist X) (A: X), 
  (G;D = P,A) -> (D= empty /\ G = P,A) \/ exists (G'': slist X), D=(G'',A).
Proof.
intros.
case_eq D; intro.
- left. intuition. rewrite H0 in H. simpl in H. assumption.
- intros.
  right. rewrite H0 in H. simpl in H.
  assert (x=A).
  + inversion H. reflexivity.
  + rewrite H1.
    exists s. reflexivity.
Qed.

Global Hint Resolve ctx_decomposition : slist.

Lemma length_snoc_distr:
  forall {X : Type} (G : slist X) (A : X), 
  slist_length (G, A) = slist_length G + 1.
Proof.
intros X G.
induction G.
- intros.
  reflexivity.
- intros.
  simpl.
  lia.
Qed.

Lemma length_snoc_min:
  forall {X : Type} (G : slist X) (A : X) (k : nat), 
  slist_length (G, A) = k -> slist_length G = k - 1.
Proof.
intros.
induction G; rewrite <- H; reflexivity.
Qed.

Lemma length_snoc_plus:
  forall {X : Type} (G : slist X) (A : X) (k : nat), 
  slist_length G = k -> slist_length (G, A) = k + 1.
Proof.
intros.
rewrite <- H.
rewrite length_snoc_distr;
intuition.
Qed.

Lemma length_empty:
  forall {X: Type} (G : slist X),
  slist_length G = 0 -> G = empty.
Proof.
intros.
destruct G.
reflexivity.
inversion H.
Qed.

(* Lemma nth_snoc:
  forall {X: Type} (n : nat) (G : slist X) (A : X),
  slist_length G = n -> (G, A)!n = Some A.
Proof.
intro X.
induction n.
- intros.
  apply length_empty in H.
  rewrite H.
  reflexivity.
- intros.
  unfold slist_nth.
  simpl.
  rewrite H.
  replace (S n - n) with (S 0).
  reflexivity.
  lia.
Qed.
 *)
 
Lemma nth_concat:
  forall {X: Type} (G' G: slist X) (k: nat),
  k = (slist_length G') -> (G;G')!k = G!0.
Proof.
intros X G'.
induction G'.
intros.
- simpl in *.
  rewrite H.
  reflexivity.
- intros.
  rewrite H.
  simpl.
  apply IHG'.
  reflexivity.
Qed.


(* Lemma nth_concat:
  forall {X: Type} (G' G: slist X) (k: nat),
  k < (slist_length G) -> (G;G')!k = G!k.
Proof.
intros X.
induction G'.
- intros.

(*   G *)
(*   inversion H. *)

(*   G' *)
  reflexivity.
- intros.
  destruct k.
  + apply IHG' in H.
    rewrite <- H.
    simpl.
  

(*   G *)
(*   change_no_check ((G, x); G') 
  with (G; ((empty, x); G')).
  assert (K := IHG ((empty, x); G') k).
  destruct K. *)
  
(*   G' *)
  change (G; (G', x)) with ((G ; G') , x).
  assert (H' := H).
(*   apply IHG' in H. *)
(*   rewrite <- H.  *)
  
  
(*   + admit.
    (* rewrite H. *)
  + 
  simpl in H.
  
  apply length_snoc_plus in H.
  assert (K := IHG _ k).
  rewrite  K;
  intuition.
  destruct K.
  admit.
  rewrite nth_snoc.
 *)Admitted.
 *)

Lemma nth_eq:
  forall {X : Type} (G D : slist X) (i : nat) (A : X), 
  G = D -> G!i = Some A -> D!i = Some A.
Proof.
intros.
rewrite H in H0.
assumption.
Qed.

Lemma length_eq:
  forall {X : Type} (D' D G : slist X) (A : X) (k : nat), 
  G = (D, A);D' -> slist_length D' = k -> G!k = Some A.
Proof.
intros.
rewrite H.
rewrite nth_concat.
intuition.
intuition.
Qed.

Lemma slist_pos_dec:
  forall {X: Type} (G: slist X) (A : X) (i: nat),
  slist_length G > i -> 
  exists (G' G'' : slist X), G = (G', A); G''
  /\ G!i = Some A.
Proof.
intros X G.
induction G.
- intros.
  inversion H.
- intros.
  
Admitted.


(* dada una lista y un i < slist_length, descomponer
la lista en 3; lo que está antes, lo que está en i y lo que está
después.

G = G';(x);G''
l(G'') = i
l(G') = l(G) - i
G!i = x
 *)

(* Lemma length_eq:
  forall {X : Type} (D D' G : slist X) (A : X) (k : nat), 
  G = (D, A);D' -> slist_length D = k -> G!k = Some A.
Proof.
intros X D.
induction D.
- intros.
  simpl in H0.
  rewrite <- H0.
  rewrite H.
  rewrite nth_concat.
  + reflexivity.
  + unfold lt.
    reflexivity.
- intros.
  rewrite H.

  assert (slist_length D = k - 1).
  apply length_snoc_min in H0.
  assumption.

  assert (K := IHD D' _ x _ eq_refl eq_refl).
  rewrite H1 in K.
  rewrite nth_concat.
  rewrite nth_snoc; intuition.

  assert (slist_length (D, x, A) = k + 1).
  apply length_snoc_plus.
  assumption.

  lia.
Qed.
 *)
(* Lemma concat_decomposition:
  forall {X : Type} (G G' D D' : slist X) (A B : X),
  G, A; G' = D, B; D' -> elem A D \/ A = B \/ elem A D'.
Proof.
intros.
case_eq G'; intro. *)

Lemma ctx_dec:
  forall {X : Type} (G G' D : slist X) (A B : X), 
(G, A);G' = D, B -> (G' = empty /\ G = D /\ A = B) 
                 \/ exists (G'': slist X),
                    (G' = (G'', B) /\ D = ((G, A);G'')).
Proof.
intros.
case_eq G'.
- intro.
  left. intuition;
  rewrite H0 in H;
  simpl in H;
  inversion H;
  intuition.
- intros.
  right.
  rewrite H0 in H.
  assert (x = B).
  inversion H.
  reflexivity.
  rewrite H1.
  exists s.
  intuition.
  inversion H.
  reflexivity.
Qed.

(* Lemma aux:
  forall {X : Type} (D D' G G' : slist X),
G = D -> G' = D' -> G;G' = D;D'.
Proof.
intros X D.
induction D.
- intros.
  subst.
  reflexivity.
- intros.
Admitted.
 *)
 
(* Lemma help:
  forall {X : Type} (G G' D D': slist X) (A B: X) (i : nat), 
  ((G, A);G')!i = Some A -> ((D, B);D')!i = Some B -> 
  A = B \/ (G, A) = (D, A) /\ G' = D'.
Admitted. *)

 
Lemma ctx_pos_dec:
  forall {X : Type} (G G' D D': slist X) (A B: X) (i : nat), 
(G, A);G' = (D, B); D' -> ((G, A);G')!i = Some A -> ((D, B);D')!i = Some B
-> A = B -> G = D /\ G' = D'.
Proof.
intros.
split.
Abort.


(* Lemma ctx_pos_dec:
  forall {X : Type} (G G' D D': slist X) (A: X) (i : nat), 
(G, A);G' = (D, A); D' -> ((G, A);G')!i = Some A -> ((D, A);D')!i = Some A
-> G = D /\ G' = D'.
Proof. *)

(* case_eq G'.
intros.
rewrite H2 in H.
simpl in H.
symmetry in H.
assert (H3 := H).
apply ctx_dec in H.
- symmetry.
  destruct H.
  + intuition.
  + destruct H.
    destruct H.
  intuition.
- intuition
intuition.
intuition.> *)











