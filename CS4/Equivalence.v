(** Equivalence between CS4 and HK4*)


Require Import Coq.Program.Equality.
Require Import ModalLogic.
Require Import Context.
Require Import Slist.
Require Export CS4Dia.
Require Export HK4Dia.



(** From Hilbert HK4 to natural deduction CS4 *)

(* Lemma nd_thyp_last: 
  forall (D G:ctx) (A:Formula), ND_Proof D (G,A) A.
Proof.
auto.
Qed.

Hint Resolve nd_thyp_last.
 *)

Lemma AxI_CS: 
  forall (D G:ctx) (A:Formula), ND_Proof D G (JTrue (A ==> A)).
Proof.
intros.
apply nd_intro.
intuition.
Qed.

Global Hint Resolve AxI_CS : EQ1.


Lemma Ax1_CS: 
  forall (D G:ctx) (A B:Formula), ND_Proof D G (JTrue (A ==> (B ==> A))).
Proof.
intros.
repeat apply nd_intro.
intuition.
Qed.

Global Hint Resolve Ax1_CS : EQ1.


Lemma Ax2_CS: 
  forall (D G:ctx) (A B:Formula), 
   ND_Proof D G (JTrue ((A ==> (A ==> B)) ==> (A ==> B))).
Proof.
intros.
repeat apply nd_intro.
eapply (nd_apply D _ A B).
- eapply (nd_apply D _ A (A ==> B)); intuition.
- intuition.
Qed.

Global Hint Resolve Ax2_CS : EQ1.


Lemma Ax3_CS: 
  forall (D G:ctx) (A B C:Formula), 
   ND_Proof D G (JTrue ((A ==> (B ==> C)) ==> (B ==> (A ==> C)))).
Proof.
intros.
repeat apply nd_intro.
eapply nd_apply.
 - eapply (nd_apply _ _ A (B ==> C)).
  + auto with CS4.
  + intuition.
- apply (nd_elem_thyps _ _ B). intuition.
Qed.

Global Hint Resolve Ax3_CS : EQ1.

Lemma Ax4_CS: 
  forall (D G:ctx) (A B C:Formula), 
   ND_Proof D G (JTrue ((B ==> C) ==> ((A ==> B) ==> (A ==> C)))).
Proof.
intros.
repeat apply nd_intro.
eapply nd_apply.
- apply nd_elem_thyps. intuition.
- apply (nd_apply _ _ A B); intuition.
Qed.

Global Hint Resolve Ax4_CS : EQ1.

Lemma Ax5_CS:
  forall (D G: ctx) (A B: Formula),
  ND_Proof D G (JTrue (A ==> B ==> (A ∧ B))).
Proof.
intros.
repeat apply nd_intro.
apply nd_andI; intuition.
Qed.

Global Hint Resolve Ax5_CS : EQ1.

Lemma Ax6_CS:
  forall (D G: ctx) (A B: Formula),
  ND_Proof D G (JTrue (A ∧ B ==> A)).
Proof.
intros.
apply nd_intro.
eapply nd_andE_l.
apply nd_elem_thyps. 
intuition.
Qed.

Global Hint Resolve Ax6_CS : EQ1.

Lemma Ax7_CS:
  forall (D G: ctx) (A B: Formula),
  ND_Proof D G (JTrue (A ∧ B ==> B)).
Proof.
intros.
apply nd_intro.
eapply nd_andE_r.
apply nd_elem_thyps.
intuition.
Qed.

Global Hint Resolve Ax7_CS : EQ1.

Lemma Ax8_CS:
  forall (D G: ctx) (A B: Formula),
  ND_Proof D G (JTrue (A ==> A ∨ B)).
Proof.
intros.
apply nd_intro.
eapply nd_orI_r. intuition.
Qed.

Global Hint Resolve Ax8_CS : EQ1.

Lemma Ax9_CS:
  forall (D G: ctx) (A B: Formula),
  ND_Proof D G (JTrue (B ==> A ∨ B)).
Proof.
intros.
apply nd_intro.
eapply nd_orI_l; intuition.
Qed.

Global Hint Resolve Ax9_CS : EQ1.

Lemma Ax10_CS:
  forall (D G: ctx) (A B C: Formula),
  ND_Proof D G (JTrue ((A ==> C) ==> (B ==> C) ==> A ∨ B ==> C)).
Proof.
intros.
repeat apply nd_intro.
eapply nd_orE.
3: { 
 apply nd_elem_thyps. apply elem_ctxhead.
}
- apply (nd_apply D _ A C); apply nd_elem_thyps; intuition.
- apply (nd_apply D _ B C); apply nd_elem_thyps; intuition.
Qed.

Global Hint Resolve Ax10_CS : EQ1.


(* Theorem 5.1 From axiomatic to natural deduction proofs.*)
(* Conjetura: 
la traduccion que se necesita para $\Diamond A$ en el sistema 
axiomatico debe ser $A\, Poss$ en el sistema de deduccion natural *)
Theorem HK_to_CS:
  forall (G: ctx) (A: Formula), (G |- A) -> (ND_Proof empty G (JTrue A)).
Proof.
intros.
dependent induction H; auto with CS4; intuition.
(* - apply nd_intro_val.
  apply nd_intro.
  apply nd_diaI.
  eapply nd_diaE.
  + apply nd_elem_thyps; intuition.
  + intuition.
  - apply nd_intro.
  apply nd_diaI.
  eapply nd_diaE.
  + apply nd_elem_thyps.
 intuition.
  + eapply nd_diaE.
    apply nd_elem_thyps; intuition.
  apply nd_tp.
intuition.    *)
- eapply nd_apply.
  apply (nd_weakening_thyps_ctxR _ _ _ IHDeriv2).
  apply (nd_weakening_thyps_ctxL _ _ _ IHDeriv1).
Qed.

Global Hint Resolve HK_to_CS : EQ1.


Corollary HK_to_CS_poss:
  forall (G: ctx) (A: Formula), 
  (G |- $A) -> (ND_Proof empty G (JPoss A)).
Proof.
intros.
apply HK_to_CS in H.
eapply nd_diaE in H.
exact H.
apply nd_tp.
rewrite <- (ctx_conc_empty (empty,A)).
apply nd_thyp.
Qed.

Global Hint Resolve HK_to_CS_poss : EQ1.


(* From CS4 natural deduction to HK4 axiomatic *)

Lemma boxtrans: 
  forall (G:ctx) (A B:Formula), 
  G |- (#(#A) ==> B) ->  G |- (#A ==> B).
Proof.
intros.
assert(K:=AxBox4 empty A).
rewrite <- (ctx_empty_conc G).
eapply trans_dett.
exact K.
exact H.
Qed.

Global Hint Resolve boxtrans : EQ1.

Lemma GenNec_swap: 
  forall (D:ctx) (A:Formula),
   boxed D |- A -> forall (G:ctx), G; boxed D |- Box A.
Proof.
intro.
induction D; auto with HK4.
Qed.
(* intros.
simpl in H.
apply DeductionTh in H.
eapply IHD in H.
apply AxBoxK_dett in H.
apply boxtrans in H.
apply inverseDT in H.
exact H.
Qed.
 *)
Global Hint Resolve GenNec_swap : EQ1.


(* Lemma 5.2 General Neccesitation *)
Lemma GenNec: 
  forall (D:ctx) (A:Formula),
   boxed D |- A -> forall (G:ctx), boxed D; G |- Box A.
Proof.
intros.
apply ctx_permutation.
intuition.
Qed.

Global Hint Resolve GenNec : EQ1.

Definition transl (A: Judgm) : Formula :=
match A with 
| JTrue B => B
| JPoss B => $B
end.


(* Theorem 5.3 From natural deduction to axiomatic proofs *)
(* Conjetura: 
el caso de $\Diamond A \,True$ debe ser traducido a $\Diamond A$ 
y el caso especial $A \,True$ debe ser traducido tambien a $\Diamond A$.
*)
Theorem CS_to_HK:
  forall (D: ctx) (G: ctx) (A: Judgm),
  (ND_Proof D G A) -> boxed D;G |- (transl A).
Proof.
intros.
dependent induction H.
- apply Hyp.
  intuition.
- rewrite boxed_conc.
  rewrite <- (ctx_empty_conc ((boxed (D, B); boxed D'); G)). 
  eapply (MP _ empty (#B) B).
  + simpl; intuition.
  + apply (AxBoxT _ B).
- apply DeductionTh.
  apply IHND_Proof; intuition.
- apply ctx_contraction.
  eapply MP.
  + apply IHND_Proof2; intuition.
  + eapply IHND_Proof1; intuition.
- apply GenNec.
  simpl in IHND_Proof.
  apply IHND_Proof; intuition.
- simpl in IHND_Proof2.
  eapply deductionTh_genPremise in IHND_Proof2; intuition.
  apply ctx_contraction.
  eapply MP.
  + apply IHND_Proof1; intuition.
  + apply IHND_Proof2.
- apply ctx_contraction.
  eapply MP.
  + exact IHND_Proof1.
  + simpl. simpl in IHND_Proof2.
    apply deductionTh_genPremise in IHND_Proof2.
    exact IHND_Proof2.
- intuition.
- simpl in *.
  apply DeductionTh in IHND_Proof2.
  eapply GenNec in IHND_Proof2.
  apply AxDiaK_dett in IHND_Proof2.
  apply (MP _ _ _ _ IHND_Proof1) in IHND_Proof2.
  apply ctx_contraction in IHND_Proof2.
  assert (boxed D; G |- (($ $ C) ==> $ C)); intuition.
  apply (MP _ _ _ _ IHND_Proof2) in H1.
  apply ctx_contraction in H1.
  exact H1.
- simpl in *.
  assert (empty |- (A ==> $A)); intuition.
  rewrite <- (ctx_empty_conc (boxed D;G)).
  apply (MP _ _ _ _ IHND_Proof H0).
- simpl in *.
  apply ctx_contraction.
  apply (MP _ _ B (A ∧ B) IHND_Proof2).
  apply ctx_contraction.
  apply (MP _ _ A (B ==> A ∧ B) IHND_Proof1).
  apply Ax5.
- simpl in *.
  apply ctx_contraction.
  apply (MP _ _ (A ∧ B) A IHND_Proof).
  apply Ax6.
- simpl in *.
  apply ctx_contraction.
  apply (MP _ _ (A ∧ B) B IHND_Proof).
  apply Ax7.
- simpl in *.
  apply ctx_contraction.
  apply (MP _ _ A (A ∨ B) IHND_Proof).
  apply Ax8.
- simpl in *.
  apply ctx_contraction.
  apply (MP _ _ B (A ∨ B) IHND_Proof).
  apply Ax9.
- simpl in *.
  apply ctx_contraction.
  apply (MP _ _ (A ∨ B) C IHND_Proof3).
  apply DeductionTh in IHND_Proof2.
  apply ctx_contraction.
  apply (MP _ _ _ _ IHND_Proof2).
  apply DeductionTh in IHND_Proof1.
  apply ctx_contraction.
  apply (MP _ _ _ _ IHND_Proof1).
  apply Ax10.
Qed.

Global Hint Resolve CS_to_HK : EQ1.




