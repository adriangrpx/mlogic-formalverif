(** Natural Deduction System for necessity from
  ''A judmental reconstruction of Modal Logic'' 
   Pfenning & Davies (2001)
 *)

Require Import Coq.Program.Equality.
Require Import ModalLogic.
Require Import Slist.
Require Import Context.

(** --------------- INFERENCE RULES --------------- *)
(** The distinction between valid and true hypotheses is expressed by 
    separate sets of hypotheses, ie two contexts as arguments for the
    hypothetical judments.
    This definition incorporates the possibility judgment in 
    dedicated rules with explicit constructors. *)
Inductive ND_Proof : ctx -> ctx -> Judgm -> Prop :=

| nd_thyp : forall (D: ctx) (G G': ctx) (A: Formula),
             ND_Proof D ((G,A) ; G') (JTrue A)

| nd_vhyp : forall (D D': ctx) (G: ctx) (B: Formula),
             ND_Proof (D,B ; D') G (JTrue B)

| nd_intro : forall (D: ctx) (G: ctx) (A B: Formula),
             ND_Proof D (G,A) (JTrue B) -> ND_Proof D G (JTrue (A ==> B))

| nd_apply : forall (D: ctx) (G: ctx) (A B: Formula),
             ND_Proof D G (JTrue (A ==> B)) -> 
             ND_Proof D G (JTrue A) -> ND_Proof D G (JTrue B)

| nd_boxI : forall (D: ctx) (G: ctx) (A: Formula),
             ND_Proof D empty (JTrue A) -> ND_Proof D G (JTrue (Box A))

| nd_boxE : forall (D: ctx) (G: ctx) (A C: Formula),
             ND_Proof D G (JTrue (Box A)) -> ND_Proof (D,A) G (JTrue C) ->
             ND_Proof D G (JTrue C)

| nd_boxEp : forall (D: ctx) (G: ctx) (A C: Formula),
             ND_Proof D G (JTrue (Box A)) -> ND_Proof (D,A) G (JPoss C) ->
             ND_Proof D G (JPoss C)

| nd_diaI : forall (D: ctx) (G: ctx) (A: Formula),
             ND_Proof D G (JPoss A) -> ND_Proof D G (JTrue (Dia A))

| nd_diaE : forall (D: ctx) (G: ctx) (A C: Formula),
             ND_Proof D G (JTrue (Dia A)) -> ND_Proof D (empty,A) (JPoss C) ->
             ND_Proof D G (JPoss C)

| nd_tp   : forall (D G: ctx) (A: Formula),
             ND_Proof D G (JTrue A) ->  ND_Proof D G (JPoss A)

| nd_andI : forall (D: ctx) (G: ctx) (A B: Formula),
            ND_Proof D G (JTrue A) -> ND_Proof D G (JTrue B) -> ND_Proof D G (JTrue (A ∧ B))

| nd_andE_l : forall (D: ctx) (G: ctx) (A B: Formula),
            ND_Proof D G (JTrue (A ∧ B)) -> ND_Proof D G (JTrue A)

| nd_andE_r : forall (D: ctx) (G: ctx) (A B: Formula),
            ND_Proof D G (JTrue (A ∧ B)) -> ND_Proof D G (JTrue B)

| nd_orI_r : forall (D: ctx) (G: ctx) (A B: Formula),
            ND_Proof D G (JTrue A) -> ND_Proof D G (JTrue (A ∨ B))

| nd_orI_l : forall (D: ctx) (G: ctx) (A B: Formula),
            ND_Proof D G (JTrue B) -> ND_Proof D G (JTrue (A ∨ B))

| nd_orE : forall (D: ctx) (G: ctx) (A B C: Formula),
            ND_Proof D (G, A) (JTrue C) -> ND_Proof D (G, B) (JTrue C) ->
            ND_Proof D G (JTrue (A ∨ B)) -> ND_Proof D G (JTrue C).


Global Hint Constructors ND_Proof : CS4.

(** --------------- STRUCTURAL RULES --------------- *)
(** Structural rules for contexts of TRUE formulas *)

Lemma nd_elem_thyps: 
  forall (D: ctx) (G : ctx) (A: Formula), elem A G -> ND_Proof D G (JTrue A).
Proof.
intros.
assert( exists G1, exists G2, G=(G1,A);G2 ).
- apply elem_ctxsplit.
  assumption.
- destruct H0 as [G1'].
  destruct H0 as [G2'].
  rewrite H0.
  apply nd_thyp.
Qed.

Global Hint Resolve nd_elem_thyps : CS4.


Lemma nd_weakening_thyps: 
  forall (D: ctx) (G G': ctx) (A: Judgm),
  ND_Proof D (G ; G') A -> forall (B : Formula), ND_Proof D (G, B ; G') A.
Proof.
intros.
dependent induction H ; eauto with CS4.
- apply nd_elem_thyps.
  assert(elem A (G;G')).
  + rewrite <- x. apply elem_conc_L. intuition.
  + apply elem_conc_split in H.
    destruct H; intuition.
  (* -- apply elem_conc_L. intuition.
  -- apply elem_conc_R; intuition. *)
- apply nd_intro.
  rewrite ctx_snoc_conc. intuition.
(*   apply (IHND_Proof G (G',A) eq_refl B0). *)
- eapply nd_orE.
  3: {
    apply IHND_Proof3. reflexivity.
  } 
  + change (((G, B0); G'), A) with ((G, B0); (G', A)).
    apply IHND_Proof1. intuition.
  + change (((G, B0); G'), B) with ((G, B0); (G', B)).
    apply IHND_Proof2. intuition.
(* - eapply nd_boxE.
  + apply IHND_Proof1.
    intuition.
  + rewrite ctx_snoc_conc.
    apply IHND_Proof2. 
    reflexivity.
- eapply nd_boxEp.
  + apply IHND_Proof1; intuition.
  + rewrite ctx_snoc_conc.
  eapply IHND_Proof2; intuition. 
 *)
Qed.

Global Hint Resolve nd_weakening_thyps : CS4.

Lemma nd_weak_last : 
  forall (D: ctx) (G: ctx) (A : Judgm),
  ND_Proof D G A -> forall (B : Formula), ND_Proof D (G, B) A.
Proof.
intros.
change (G,B) with ((G,B);empty).
apply nd_weakening_thyps. intuition.
(* simpl.
assumption. *)
Qed.

Global Hint Resolve nd_weak_last : CS4. 


Lemma nd_intro_inv: 
  forall (D G:ctx) (A B:Formula), 
  ND_Proof D G (JTrue (A ==> B)) -> ND_Proof D (G,A) (JTrue B).
Proof.
intros.
eapply nd_apply with A.
- change (G,A) with ((G,A);empty).
  apply nd_weakening_thyps.
  assumption.
(* - intuition. *)
- intuition.
  (* apply nd_elem_thyps. apply elem_ctxhead *)
Qed.

Global Hint Resolve nd_intro_inv : CS4.


Lemma nd_introDia_inv:
  forall (D G: ctx) (A: Formula),
  ND_Proof D G (JTrue ($ A)) -> ND_Proof D G (JPoss A).
Proof.
intros.
eapply nd_diaE in H.
exact H.
apply nd_tp.
apply nd_elem_thyps.
intuition.
(* apply elem_ctxhead. *)
Qed.

Global Hint Resolve nd_introDia_inv : CS4.


Lemma nd_exchange_thyps_True: 
  forall (D G:ctx) (A B C: Formula),
  ND_Proof D ((G,A),B) (JTrue C) -> ND_Proof D ((G,B),A) (JTrue C).
Proof.
intros.
apply (nd_apply _ _ B C).
- apply nd_intro.
  change (((G, B), A), B) with ((G, B);((empty, A), B)).
  apply nd_weakening_thyps. intuition.
- intuition.
(* - apply nd_elem_thyps. apply elem_ctxcons. intuition. *)
Qed.

Global Hint Resolve nd_exchange_thyps_True : CS4.


Lemma nd_exchange_thyps_Poss: 
  forall (D G:ctx) (A B C: Formula),
  ND_Proof D ((G,A),B) (JPoss C) -> ND_Proof D ((G,B),A) (JPoss C).
Proof.
intros.
dependent induction H.
- eapply nd_boxEp.
  + apply nd_exchange_thyps_True in H.
    exact H.
  + apply IHND_Proof2; intuition.
- eapply nd_diaE.
  + apply nd_exchange_thyps_True in H.
    exact H.
  + exact H0.
- apply nd_exchange_thyps_True in H.
  apply nd_tp.
  assumption.
Qed.

Global Hint Resolve nd_exchange_thyps_Poss : CS4.

Lemma nd_exchange_thyps: 
  forall (C: Judgm) (D G:ctx) (A B: Formula),
  ND_Proof D ((G,A),B) C -> ND_Proof D ((G,B),A) C.
Proof.
intro.
induction C;  intros.
- apply nd_exchange_thyps_True in H; intuition.
- apply nd_exchange_thyps_Poss in H; intuition. 
Qed.

Global Hint Resolve nd_exchange_thyps : CS4.


Lemma nd_weakening_thyps_ctxR: 
  forall (D: ctx) (G : ctx) (A: Judgm), 
  ND_Proof D G A -> forall (G': ctx), ND_Proof D (G;G') A.
Proof.
intros.
induction G'; auto.
eapply nd_weak_last in IHG'.
exact IHG'.
Qed.

Global Hint Resolve nd_weakening_thyps_ctxR : CS4.

Lemma nd_weakening_thyps_ctxL:
  forall (G: ctx) (D : ctx) (A: Judgm), 
  ND_Proof D G A -> forall (G': ctx), ND_Proof D (G';G) A.
Proof.
intro.
induction G.
- intros.
  simpl.
  rewrite <- (ctx_empty_conc G').
  apply nd_weakening_thyps_ctxR.
  assumption.
- intros.
  simpl.
  dependent induction H; eauto with CS4.
  apply nd_intro_inv.
  apply IHG.
  apply nd_intro.
  rewrite <- x.
  intuition.
Qed.

Global Hint Resolve nd_weakening_thyps_ctxL : CS4.


Lemma nd_exch_thyps_conc_True: 
  forall (G' D G:ctx) (A B: Formula),
   ND_Proof D ((G,A);G') (JTrue B) -> ND_Proof D (G;(G',A)) (JTrue B).
Proof.
intro.
induction G'.
- auto.
- intros.
  simpl in H.
  apply nd_intro in H.
  apply IHG' in H.
  apply nd_intro_inv in H.
  change (G; ((G', x), A)) with (((G; G'), x), A).
  change ((G; (G', A)), x) with (((G; G'), A), x) in H.
  apply nd_exchange_thyps_True.
  assumption.
Qed.

Global Hint Resolve nd_exch_thyps_conc_True : CS4.

Lemma nd_exch_thyps_conc_Poss: 
  forall (G' D G:ctx) (A B: Formula),
   ND_Proof D ((G,A);G') (JPoss B) -> ND_Proof D (G;(G',A)) (JPoss B).
Proof.
intros.
dependent induction H.
- eapply nd_boxEp.
  + apply nd_exch_thyps_conc_True in H.
    exact H.
  + apply IHND_Proof2; intuition.
- eapply nd_diaE.
  + apply nd_exch_thyps_conc_True in H.
    exact H.
  + exact H0.
- apply nd_exch_thyps_conc_True in H.
  auto with CS4.
Qed.

Global Hint Resolve nd_exch_thyps_conc_Poss : CS4.

Lemma nd_exch_thyps_conc: 
  forall (B: Judgm) (G' D G:ctx) (A: Formula),
   ND_Proof D ((G,A);G') B -> ND_Proof D (G;(G',A)) B.
Proof.
intros.
case_eq B; intros; rewrite H0 in H.
apply nd_exch_thyps_conc_True; intuition.
apply nd_exch_thyps_conc_Poss; intuition.
Qed.

Global Hint Resolve nd_exch_thyps_conc : CS4.

Lemma nd_intro_gen: 
  forall (G' D G:ctx) (A B:Formula),
   ND_Proof D (G,A;G') (JTrue B) -> ND_Proof D (G;G') (JTrue (A ==> B)).
Proof.
intro.
induction G'.
- intros. auto with CS4.
- intros. 
  simpl in H.
  apply nd_intro.
  apply nd_exch_thyps_conc_True.
  assumption.
Qed.

Global Hint Resolve nd_intro_gen : CS4.

Lemma nd_intro_gen_inv: 
  forall (G' D G:ctx) (A B:Formula),
  ND_Proof D (G;G') (JTrue (A ==> B)) -> ND_Proof D (G,A;G') (JTrue B).
Proof.
intros.
apply nd_intro_inv in H.
eauto with CS4. (*FIXME*)
Qed.

Global Hint Resolve nd_intro_gen_inv : CS4.


Lemma nd_exch_thyps_snoc_True: 
 forall (G' D G:ctx) (A B: Formula),
 ND_Proof D (G;(G',A)) (JTrue B) -> ND_Proof D ((G,A);G') (JTrue B).
Proof.
intro.
induction G'.
- auto.
- intros.
  simpl.
  apply nd_intro_inv.
  apply IHG'.
  simpl.
  apply nd_intro.
  apply nd_exchange_thyps_True.
  simpl in H.
  assumption.
Qed.

Global Hint Resolve nd_exch_thyps_snoc_True : CS4.


Lemma nd_exch_thyps_snoc_Poss: 
 forall (G' D G:ctx) (A B: Formula),
 ND_Proof D (G;(G',A)) (JPoss B) -> ND_Proof D ((G,A);G') (JPoss B).
Proof.
intros.
dependent induction H; eauto with CS4.
Qed.

Global Hint Resolve nd_exch_thyps_snoc_Poss : CS4.


Lemma nd_exch_thyps_snoc: 
 forall (B: Judgm) (G' D G:ctx) (A: Formula) ,
 ND_Proof D (G;(G',A)) B -> ND_Proof D ((G,A);G') B.
Proof.
intro.
case B; intros.
- apply nd_exch_thyps_snoc_True; intuition.
- apply nd_exch_thyps_snoc_Poss; intuition.
Qed.

Global Hint Resolve nd_exch_thyps_snoc : CS4.




Theorem nd_subst_1:
  forall (D G: ctx) (A: Formula), (ND_Proof D G (JTrue A)) ->
  forall (G':ctx) (C: Formula),
    (ND_Proof D ((G,A);G') (JTrue C)) -> ND_Proof D (G;G') (JTrue C).
Proof.
intros.
apply nd_intro_gen in H0.
eapply nd_apply.
exact H0.
apply nd_weakening_thyps_ctxR.
assumption.
Qed.

Global Hint Resolve nd_subst_1 : CS4.

(** Structural rules for contexts of VALID formulas *)

Lemma nd_elem_vhyps: 
  forall (D: ctx) (G : ctx) (A: Formula), elem A D -> ND_Proof D G (JTrue A).
Proof.
intros.
assert( exists D1, exists D2, D=(D1,A);D2 ).
- apply elem_ctxsplit.
  assumption.
- destruct H0 as [D1'].
  destruct H0 as [D2'].
  rewrite H0.
  apply nd_vhyp.
Qed.

Global Hint Resolve nd_elem_vhyps : CS4.


Lemma nd_weakening_vhyps : 
  forall (D D': ctx) (G: ctx) (A: Judgm),
  ND_Proof (D ; D') G A -> forall (B : Formula), ND_Proof (D, B ; D') G A.
Proof.
intros.
dependent induction H; eauto with CS4.
(* - apply nd_thyp. *)
- apply nd_elem_vhyps.
  assert(elem B (D;D')).
  + rewrite <-x. apply elem_conc_L. intuition.
  + apply elem_conc_split in H.
    destruct H.
    * apply elem_conc_L.
      intuition.
    * apply elem_conc_R. 
    assumption.
(* - apply nd_intro.
  apply IHND_Proof.
  intuition.
- eapply nd_apply.
  + apply IHND_Proof1; intuition.
  + apply IHND_Proof2; intuition. 
- apply nd_boxI.
  apply IHND_Proof.
  intuition. *)
- eapply nd_boxE.
  + apply IHND_Proof1.
    intuition.
  + rewrite ctx_snoc_conc.
    apply IHND_Proof2. 
    reflexivity.
- eapply nd_boxEp.
  + apply IHND_Proof1; intuition.
  + rewrite ctx_snoc_conc.
  eapply IHND_Proof2; intuition.
Qed.

Global Hint Resolve nd_weakening_vhyps : CS4.



Lemma nd_weak_lastV : 
  forall (D: ctx) (G: ctx) (A: Judgm),
  ND_Proof D G A -> forall (B: Formula), ND_Proof (D, B) G A.
Proof.
intros.
assert (ND_Proof ((D,B);empty) G A); eauto with CS4.
Qed.

Global Hint Resolve nd_weak_lastV : CS4.


(* Proposition 3.13 Inversion of Implication introduction for validity*)
Lemma nd_intro_val_inv: 
  forall (D G: ctx) (A B: Formula),
  ND_Proof D G (JTrue (#A ==> B)) -> ND_Proof (D,A) G (JTrue B).
Proof.
intros.
eapply nd_apply.
apply nd_weak_lastV.
exact H.
apply nd_boxI.
change (D,A) with ((D,A);empty).
apply nd_vhyp.
Qed.


Global Hint Resolve nd_intro_val_inv : CS4.


Lemma nd_exchange_vhyps_True: 
  forall (D G:ctx) (A B C: Formula),
  ND_Proof ((D,A),B) G (JTrue C) -> ND_Proof ((D,B),A) G (JTrue C).
Proof.
intros.
apply (nd_apply _ _ (# B) C).
- apply nd_intro.
  eapply nd_boxE.
  + change (G, (#B)) with ((G,(#B));empty).
     eapply nd_thyp.
  + apply nd_weak_last.
  change (((D, B), A), B) with ((D, B);((empty, A), B)).
  apply nd_weakening_vhyps.
  intuition.
- apply nd_boxI. 
  apply nd_elem_vhyps.
  intuition.
  (* apply elem_ctxcons. apply elem_ctxhead. *)
Qed.

Global Hint Resolve nd_exchange_vhyps_True : CS4.


Lemma nd_weakening_vhyps_ctxR:
  forall (D: ctx) (G : ctx) (A: Judgm), 
  ND_Proof D G A -> forall (D': ctx), ND_Proof (D;D') G A.
Proof.
intros.
induction D'; auto.
assert (ND_Proof ((D;D'), x) G A); auto with CS4.
Qed.

Global Hint Resolve nd_weakening_vhyps_ctxR : CS4.


Lemma nd_weakening_vhyps_ctxL:
  forall (D: ctx) (G : ctx) (A: Judgm), 
  ND_Proof D G A -> forall (D': ctx), ND_Proof (D';D) G A.
Proof.
intro.
induction D'; intros; auto with CS4.
rewrite ctx_empty_conc.
assumption.
Qed.

Global Hint Resolve nd_weakening_vhyps_ctxL : CS4.


Theorem nd_subst_2:
  forall (D G: ctx) (A: Formula), (ND_Proof D G (JTrue A)) ->
  forall (G':ctx) (C: Formula),
    (ND_Proof D ((G,A);G') (JPoss C)) -> ND_Proof D (G;G') (JPoss C).
Proof.
intros.
dependent induction H0. (* ; eauto. *)
- eapply nd_boxEp.
  + eapply nd_apply.
    -- apply nd_intro_gen.
       exact H0_.
    -- apply nd_weakening_thyps_ctxR.
       assumption.
  + eapply IHND_Proof2.
    -- eapply nd_weak_lastV.
       exact H.
    -- intuition.
    -- intuition.
- eapply nd_diaE.
  + eapply nd_apply.
    -- apply nd_intro_gen.
       exact H0_.
    -- apply nd_weakening_thyps_ctxR.
       exact H.
  + exact H0_0.
- eauto with CS4. (*FIXME*)
Qed.

Global Hint Resolve nd_subst_2 : CS4.

Theorem nd_subst_5:
  forall (D G: ctx) (A: Formula), (ND_Proof D G (JPoss A)) ->
  forall (C: Formula),
    (ND_Proof D (empty, A) (JPoss C)) -> ND_Proof D G (JPoss C).
Proof.
(* Direct proof, subst5 is consequence of diaE, eauto ok *)
intros.
eapply nd_diaE.
- apply nd_diaI.
  exact H.
- exact H0.
Qed.

(* Old proof by induction *)
(* intros.
dependent induction H.
- eapply nd_boxEp. exact H. eapply IHND_Proof2. reflexivity. 
  rewrite <- (ctx_conc_empty D) in H1.
  apply (nd_weakening_vhyps D empty _ _ H1 A0).
- eapply nd_diaE. exact H. eapply IHND_Proof2. reflexivity. exact H1.
- rewrite <- (ctx_conc_empty G).
eapply nd_subst_2. exact H.
change ((G, A); empty) with (G;(empty, A)).
apply nd_weakening_thyps_ctxL.
exact H0.
Qed. *)


(** 
  Transference relationship of formulas from one context to another *)

(** Valid formulas are necessary truths *)
Proposition val_to_true: 
  forall (G D:ctx) (A B: Formula),
   ND_Proof (D,A) G (JTrue B) -> ND_Proof D (G, #A) (JTrue B).
Proof.
(* prueba del revisor 3*)
intros.
remember (nd_boxE D (G, # A) A B) as n.
(* pose (nd_boxE D (G, # A) A B). *)
apply n.
(* intuition. *)
remember (nd_thyp D G empty (# A)) as n0. 
simpl in n0.
exact n0.
apply nd_weak_last.
exact H.
(* nuestra prueba 
intros.
dependent induction H.
- auto.
- assert(K:=x); apply ctx_decomposition in x.
  destruct x.
  + destruct H.
    inversion H0.
    eapply nd_apply.
    * eapply Axiom_T.
    * intuition.
  + destruct H.
    rewrite H in K.
    simpl in K.
    inversion K.
    intuition.
- apply nd_intro.
  apply nd_exchange_True.
  apply (nd_boxE D _ A B);  intuition.
- eapply nd_apply.
  + apply IHND_Proof1; intuition.
  + apply IHND_Proof2; intuition.
- apply (nd_boxE D (G,(#A)) (A) (#A0)).
  + intuition.
  + intuition.
- apply (nd_boxE D (G,(#A)) A C).
  + intuition.
  + change (G,(#A)) with (G;(empty,#A)).
    eapply (nd_subst_1).
    * exact H.
    * simpl.
      apply nd_weak_last.
      apply IHND_Proof2.
      reflexivity.
*)
Qed.

Global Hint Resolve val_to_true : CS4.


(* Corollary 3.11 *)
Corollary ctx_val_to_true:
  forall (D G: ctx) (A: Formula),
  ND_Proof D G (JTrue A) -> ND_Proof empty (boxed D; G) (JTrue A).
Proof.
intro.
induction D; auto with CS4.
intros.
simpl.
apply val_to_true in H.
apply IHD in H.
simpl in H.
apply nd_exch_thyps_snoc_True.
assumption.
Qed.

Global Hint Resolve ctx_val_to_true : CS4.


(* Corollary 3.12 Implication introduction for validity *)
Corollary nd_intro_val: 
  forall (D G: ctx) (A B: Formula),
  ND_Proof (D,A) G (JTrue B) -> ND_Proof D G (JTrue (#A ==> B)).
Proof.
intros.
auto with CS4.  (* el auto solo funciona con ctx_val_to_true*)
Qed.

Global Hint Resolve nd_intro_val: CS4.


Lemma nd_exchange_vhyps_Poss: 
  forall (D G:ctx) (A B C: Formula),
  ND_Proof ((D,A),B) G (JPoss C) -> ND_Proof ((D,B),A) G (JPoss C).
Proof.
intros.
dependent induction H. (*; eauto.*) 
- apply nd_diaI in H0.
  apply nd_intro_val in H0.
  apply nd_exchange_vhyps_True in H0.
  apply nd_intro_val_inv in H0.
  apply nd_introDia_inv in H0.
  apply nd_exchange_vhyps_True in H.
  apply (nd_boxEp ((D,B),A) G A0 C H) in H0.
  exact H0.
- apply (nd_diaE ((D,A),B) G A0 C H) in H0.
  apply nd_diaI in H0.
  apply nd_exchange_vhyps_True in H0.
  apply nd_introDia_inv in H0.
  exact H0.
- apply nd_exchange_vhyps_True in H.
  apply nd_tp.
  exact H.
Qed.

Global Hint Resolve nd_exchange_vhyps_Poss : CS4.


Lemma nd_intro_val_gen:
  forall (D D' G: ctx) (A B: Formula),
  ND_Proof ((D,A);D') G (JTrue B) -> ND_Proof (D;D') G (JTrue (#A ==> B)).
Proof.
intros.
dependent induction D'; auto with CS4.

apply nd_intro_val.
simpl.
apply nd_exchange_vhyps_True.
simpl in H.
 
eapply val_to_true in H.
eapply nd_intro in H.
apply IHD' in H.
repeat apply nd_intro_val_inv.
assumption.
Qed.

Global Hint Resolve nd_intro_val_gen : CS4.

Theorem nd_subst_3:
  forall (D: ctx) (B: Formula), (ND_Proof D empty (JTrue B)) ->
  forall (D' G:ctx) (C: Formula),
    (ND_Proof ((D,B);D') G (JTrue C)) -> ND_Proof (D;D') G (JTrue C).
Proof.
intros.
eapply nd_apply.
- eapply (nd_boxI D G) in H.
  apply nd_intro_val_gen in H0.
  exact H0.
- apply nd_weakening_vhyps_ctxR.
  auto with CS4.
Qed.

Global Hint Resolve nd_subst_3 : CS4.


Proposition val_to_true_Poss: 
  forall (G D:ctx) (A B: Formula),
   ND_Proof (D,A) G (JPoss B) -> ND_Proof D (G, #A) (JPoss B).
Proof.
intros. 
dependent induction H; auto with CS4; intros.
- eapply nd_boxEp.
 + apply val_to_true in H.
   apply H.
 + apply nd_diaI in H0.
   apply nd_intro_val in H0.
   apply val_to_true in H0.
   apply nd_intro_val_inv in H0.
   apply nd_introDia_inv in H0.
   exact H0.
- apply nd_introDia_inv.
  apply val_to_true.
  apply (nd_diaE (D,A) G A0 B H) in H0.
  intuition.
Qed.

Global Hint Resolve val_to_true_Poss : CS4.


Corollary ctx_val_to_true_Poss:
  forall (D G: ctx) (A: Formula),
  ND_Proof D G (JPoss A) -> ND_Proof empty (boxed D; G) (JPoss A).
Proof.
intro.
induction D.
- intros; intuition.
- intros.
simpl.
apply val_to_true_Poss in H. 
apply IHD in H.
simpl in H.
apply nd_exch_thyps_snoc_Poss.
assumption.
Qed.

Global Hint Resolve ctx_val_to_true_Poss : CS4.


(* Corollary 3.15  *)
Corollary ctx_true_to_val:
  forall (D G: ctx) (A: Formula),
  ND_Proof empty (boxed D; G) (JTrue A) -> ND_Proof D G (JTrue A).
Proof.
intro.
induction D.
- simpl.
  intros.
  rewrite (ctx_empty_conc G) in H.
  assumption.
- intros.
  simpl in H.
  apply nd_intro_gen in H.
  apply IHD in H.
  apply nd_intro_val_inv in H.
  assumption.
Qed.

Global Hint Resolve ctx_true_to_val : CS4.


(* Proposition 3.14 Neccesary truths are valid  *)
Proposition true_to_val_True: 
  forall (D G: ctx) (A B: Formula),
    ND_Proof D (G, #A) (JTrue B) -> ND_Proof (D,A) G (JTrue B).
Proof.
intros.
apply nd_intro_val_inv.
apply nd_intro.
assumption.
Qed.

Global Hint Resolve true_to_val_True : CS4.




Lemma nd_intro_val_gen_inv:
  forall (D D' G: ctx) (A B: Formula),
  ND_Proof (D;D') G (JTrue (#A ==> B)) -> ND_Proof ((D,A);D') G (JTrue B).
Proof.
intros D D' G A.
induction D'; simpl.
- intuition.
- intros.
  eapply nd_intro_inv in H.
  intuition.
(*   apply true_to_val_True in H.
  simpl in H.
  apply nd_exchange_vhyps_True in H.
  repeat (apply nd_intro_val in H).
  apply IHD' in H.
  apply nd_intro_val_inv in H.
  simpl.
  intuition.
 *)
Qed.

Global Hint Resolve nd_intro_val_gen_inv : CS4.



Lemma nd_exch_vhyps_snoc_True: 
 forall (D' D G:ctx) (A B: Formula),
 ND_Proof (D;(D',A)) G (JTrue B) -> ND_Proof ((D,A);D') G (JTrue B).
Proof.
intros.
dependent induction D'; auto with CS4.
(* simpl in H.
apply nd_intro_val in H.
change ((D; D'), f) with (D; (D', f)) in H.
apply nd_intro_val_gen_inv in H. 
assumption.
 *)
Qed.

Global Hint Resolve nd_exch_vhyps_snoc_True : CS4.



Lemma nd_exch_vhyps_snoc_Poss: 
 forall (D' D G:ctx) (A B: Formula),
 ND_Proof (D;(D',A)) G (JPoss B) -> ND_Proof ((D,A);D') G (JPoss B).
Proof.
intro.
induction D'.
- intros.
  intuition.
- intros.
simpl in H.
apply nd_exchange_vhyps_Poss in H.
apply nd_diaI in H.
repeat (apply nd_intro_val in H).
apply nd_intro_val_gen_inv in H.
apply nd_intro_val_inv in H.
apply nd_introDia_inv in H.
assumption.
Qed.

Global Hint Resolve nd_exch_vhyps_snoc_Poss : CS4.




Proposition true_to_val_Poss: 
  forall (D G: ctx) (A B: Formula),
    ND_Proof D (G, #A) (JPoss B) -> ND_Proof (D,A) G (JPoss B).
Proof.
intros.
dependent induction H. (*; eauto.*)
- eapply nd_boxEp.
  + apply true_to_val_True in H.
    exact H.
  + apply nd_exchange_vhyps_Poss.
    apply IHND_Proof2; intuition.
- apply (nd_diaE D (G, (#A)) A0 B H) in H0.
  apply nd_diaI in H0.
  apply nd_intro in H0.
  apply nd_intro_val_inv in H0.
  apply nd_introDia_inv in H0.
  assumption.
- apply true_to_val_True in H.
  apply nd_tp in H.
  assumption.
Qed.

Global Hint Resolve true_to_val_Poss : CS4.


Lemma nd_exch_vhyps_conc_True: 
 forall (D' D G:ctx) (A B: Formula),
 ND_Proof ((D,A);D') G (JTrue B) -> ND_Proof (D;(D',A)) G (JTrue B).
Proof.
intros.
simpl.
apply true_to_val_True.
apply nd_intro_val_gen in H.
apply nd_intro_inv.
assumption.
Qed.

Global Hint Resolve nd_exch_vhyps_conc_True : CS4.

Lemma nd_exch_vhyps_conc_Poss: 
 forall (D' D G:ctx) (A B: Formula),
 ND_Proof ((D,A);D') G (JPoss B) -> ND_Proof (D;(D',A)) G (JPoss B).
Proof.
intros.
simpl.
apply nd_introDia_inv.
apply nd_diaI in H.
apply nd_intro_val_gen in H.
apply nd_intro_val_inv.
assumption.
Qed.

Global Hint Resolve nd_exch_vhyps_conc_Poss : CS4.


Theorem nd_subst_4:
  forall (D: ctx) (B: Formula), (ND_Proof D empty (JTrue B)) ->
  forall (D' G:ctx) (C: Formula),
    (ND_Proof ((D,B);D') G (JPoss C)) -> ND_Proof (D;D') G (JPoss C).
Proof.
intros.
dependent induction H0. (*; eauto. *)
- eapply nd_boxEp.
  + eapply nd_weakening_vhyps_ctxR in H.
    eapply nd_boxI in H.
    exact H.
  + eapply (nd_boxEp _ _ _ _ H0_) in H0_0.
    eapply nd_exch_vhyps_conc_Poss in H0_0. 
    simpl in H0_0.
    assumption.
- eapply (nd_diaE _ _ _ _ H0_) in H0_0.
  eapply nd_boxEp.
  + eapply nd_weakening_vhyps_ctxR.
    eapply nd_boxI in H.
    exact H.
  + eapply nd_exch_vhyps_conc_Poss.
    assumption.
- eauto with CS4.  (*FIXME*)
Qed.

Global Hint Resolve nd_subst_4 : CS4.






(** 
 Modal Axioms that characterize the logic S4 
 they are derivable in CS4
 *)

Theorem Axiom_T: 
  forall (D G: ctx) (A:Formula), ND_Proof D G (JTrue ((#A) ==> A)).
Proof.
intros.
apply nd_intro.
eapply nd_boxE; intuition.
(* - intuition.
- intuition.
  apply nd_elem_vhyps.
  apply elem_ctxhead. *)
Qed.

Global Hint Resolve Axiom_T : CS4.


Theorem Axiom_4: 
  forall (D G: ctx) (A:Formula), ND_Proof D G (JTrue (#A ==> ##A)).
Proof.
intros.
apply nd_intro.
eapply nd_boxE. 
- apply (nd_elem_thyps _ _ (#A)). 
  apply elem_ctxhead.
- intuition.
Qed.

Global Hint Resolve Axiom_4 : CS4.


Theorem Axiom_K: 
  forall (D G: ctx) (A B:Formula),
  ND_Proof D G (JTrue ((#(A ==> B)) ==> ((#A) ==> (#B)))).
Proof.
intros.
repeat (apply nd_intro).
apply (nd_boxE D ((G, (# (A ==> B))), (# A)) A (#B)).
- intuition. 
- apply (nd_boxE _ _ (A ==> B) (#B)).
  * intuition.
  * apply nd_boxI.
    apply (nd_apply _ _ A B); intuition. 
Qed.

Global Hint Resolve Axiom_K : CS4.

Theorem Axiom_D:
  forall (D G: ctx) (A: Formula),
  ND_Proof D G (JTrue (#A ==> $A)).
Proof.
intros.
(* eauto. *)
apply nd_intro_val.
apply nd_diaI.
apply nd_tp.
rewrite <- (ctx_conc_empty (D,A)).
apply nd_vhyp.
Qed.

Global Hint Resolve Axiom_D : CS4.


Theorem Axiom_DiaK:
  forall (D G: ctx) (A B: Formula),
  ND_Proof D G (JTrue ((#(A ==> B)) ==> ($A ==> $B))).
Proof.
intros.
apply nd_intro_val.
apply nd_intro.
apply nd_diaI.
eapply nd_diaE.
- rewrite <- (ctx_conc_empty (G,($A))). apply nd_thyp.
- apply nd_tp.
  apply (nd_apply _ _ A B)
  + apply nd_elem_vhyps. intuition. (* apply elem_ctxhead. *)
  + apply nd_elem_thyps. intuition. (* apply elem_ctxhead. *)
Qed.

Global Hint Resolve Axiom_DiaK : CS4.


Theorem Axiom_Dia4:
  forall (D G: ctx) (A: Formula),
  ND_Proof D G (JTrue ($$A ==> $A)).
Proof.
intros.
apply nd_intro.
apply nd_diaI.
eapply nd_diaE.
- 
rewrite <- (ctx_conc_empty (G,$$A)).
apply nd_thyp.
-
apply (nd_diaE _ _ A A).
+ rewrite <- (ctx_conc_empty (empty,$A)).
  apply nd_thyp.
+ apply nd_tp. auto with CS4. 
Qed.

Global Hint Resolve Axiom_Dia4 : CS4.


Theorem Axiom_DiaT:
  forall (D G: ctx) (A: Formula),
  ND_Proof D G (JTrue (A ==> $A)).
Proof.
intros.
apply nd_intro.
apply nd_diaI.
apply nd_tp.
rewrite <- (ctx_conc_empty (G,A)).
apply nd_thyp.
Qed.

Global Hint Resolve Axiom_DiaT : CS4.
