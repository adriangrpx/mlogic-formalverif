
(** Examples ----------------------------- *)
(** Most of them are rules proposed in 
 Dual-context Calculi for Modal Logic (Technical Report)
 G. A. Kavvos St John’s College
 University of Oxford
 2018
*)


Require Import Coq.Program.Equality.
Require Import ModalLogic.
Require Import Context.
Require Import Slist.
Require Export CS4Dia.
Require Export HK4Dia.
Require Export Equivalence.

(** Attempt to prove the invalid theorem 
    empty | empty |- (A ==> #A) *)
Proposition false_theorem:
  forall (A: Formula), ND_Proof empty empty (JTrue (A ==> #A)).
Proof.
intro.
apply nd_intro.
(* Two options to prove ND_Proof empty (empty, A) (# A) 
   because of the form of the conclusion*)
(* 1. 
apply nd_boxI.
which demands to prove ND_Proof empty empty A *)
(* 2. 
eapply nd_boxE.
which demands to prove ND_Proof empty (empty, A) (# ?A)
and ND_Proof (empty, ?A) (empty, A) (# A)
two boxed formulas!!! *)
Abort.


(* Transitivity *)
Lemma trans_dett:
  forall (D G: ctx)(A B C : Formula),
  ND_Proof D G (JTrue (A ==> B)) -> 
  ND_Proof D G (JTrue (B ==> C)) -> 
  ND_Proof D G (JTrue (A ==> C)).
Proof.
intros.
apply nd_intro.
rewrite <- (ctx_conc_empty G) in H,H0.
eapply nd_weakening_thyps in H.
eapply nd_weakening_thyps in H0.
assert (ND_Proof D (G,A) (JTrue A)); intuition.
apply (nd_apply _ _ _ _ H) in H1.
apply (nd_apply _ _ _ _ H0) in H1.
assumption.
Qed.


(* Scott's rule and weakening *)
Proposition intro_boxk: 
  forall (D: ctx) (A: Formula), 
  ND_Proof empty D (JTrue A) ->
  forall (G: ctx), ND_Proof D G (JTrue (#A)).
Proof.
intro.
induction D.
- intros. apply nd_boxI. assumption.
- intros.
  apply nd_intro_val_inv.
  rewrite <- (ctx_conc_empty G).
  eapply (nd_subst_1 _  _ (#(x==>A) ==> #x ==> #A)); intuition.
  eapply nd_apply.
  + apply nd_thyp.
  + apply nd_intro in H.
  eapply IHD in H.
  exact H.
Qed.


Proposition intro_boxk4:
  forall (D: ctx) (A: Formula), ND_Proof D D (JTrue A) ->
  forall (G: ctx), ND_Proof D G (JTrue (#A)).
Proof.
intro.
induction D.
- intros. rewrite <- (ctx_conc_empty G).
  apply (nd_weakening_thyps_ctxL). 
  apply nd_boxI. 
  assumption.
- intros.
  rewrite <- (ctx_conc_empty (D,x)).
  eapply (nd_subst_3 _ (#x)).
  intuition.
  apply nd_intro_val_inv.
  apply true_to_val_True.
  apply nd_intro_val.
  apply nd_intro_inv.
  rewrite <- (ctx_conc_empty G).
  eapply (nd_apply _ _ (#(x ==> A)) (#x ==> #A)).
  apply Axiom_K.
  apply nd_intro_val_inv.
  eapply (nd_apply _ _ (#(#x ==> x ==>A))); intuition.
Qed.


Proposition transfer_withoutbox_True:
  forall (G D: ctx) (A B: Formula), 
  ND_Proof D (G, A) (JTrue B) -> ND_Proof (D,A) G (JTrue B).
Proof.
intros.
apply nd_intro in H.
apply nd_intro_val_inv.
assert (Ht := Axiom_T D G A).
eapply trans_dett.
exact Ht.
exact H.
Qed.


(* Proposition transfer_withoutbox_Poss:
  forall (G D: ctx) (A B: Formula), 
  ND_Proof D (G, A) (JPoss B) -> ND_Proof (D,A) G (JPoss B).
 *)

Proposition ctx_transfer_withoutbox_True:
  forall (G G' D: ctx) (A: Formula), 
  ND_Proof D (G;G') (JTrue A) -> ND_Proof (D;G) G' (JTrue A).
Proof.
intro.
induction G.
- intros. 
  simpl.
  rewrite ctx_empty_conc in H.
  assumption.
- intros.
  apply nd_intro_gen in H.
  apply IHG in H.
  eapply nd_intro_inv in H.
  simpl.
  apply true_to_val_True.
  apply transfer_withoutbox_True in H.
  intuition.
Qed.


(** The next examples are for a Hilbert style system 
Kavvos section 2.5.2 
*)

Theorem Scott:
  forall (G: ctx) (A: Formula), G |- A -> boxed G |- #A.
Proof. 
intros.
dependent induction H; auto with HK4; intuition.
- rewrite boxed_conc.
  apply (MP _ _ (#A) (#B)); intuition.
Qed.

Theorem T_rule:
  forall (G: ctx) (A: Formula), G |- A -> boxed G |- A.
Proof.
intros.
dependent induction H; auto with HK4; intuition.
- assert (empty |- ((#A) ==> A)); intuition.
  rewrite <- (ctx_empty_conc (boxed G)).
  apply (MP _ _ (#A) A); intuition.
- rewrite boxed_conc.
  apply (MP _ _ A B); intuition.
Qed.


Lemma double_boxed_ctx:
forall (G G': ctx) (A: Formula), G'; boxed (boxed G) |- A -> G';(boxed G) |- A.
Proof.
intro.
induction G; intuition.
simpl.
apply inverseDT.
simpl in H.
apply DeductionTh in H.
 apply boxtrans in H.
 apply IHG.
 assumption.
 Qed.
 
Global Hint Resolve double_boxed_ctx : HK4.


Theorem Four_rule:
  forall (G: ctx) (A: Formula), (boxed G); G |- A -> boxed G |- (#A).
Proof.
intros.
dependent induction H; auto with HK4.
- apply elem_conc_split in H. destruct H; auto with context; intuition.
- apply Scott in H0.
  assert (empty |- (#(A==>B) ==> #A ==> #B)); intuition.
  apply (MP (boxed G') empty (#(A==>B)) (#A ==> #B)) in H0; intuition.
  apply Scott in H.
  apply (MP _ _ _ _ H) in H0. 
  rewrite ctx_empty_conc in H0.
  rewrite <- boxed_conc in H0.
  rewrite x in H0.
  rewrite boxed_conc in H0.
  apply ctx_permutation in H0.
  apply double_boxed_ctx in H0.
  apply ctx_contraction in H0.
  exact H0.
Qed.
    
Global Hint Resolve Four_rule : HK4.

Corollary Four_cor:
  forall (G: ctx) (A: Formula), (boxed G) |- A -> boxed G |- (#A).
Proof.
intros; auto with HK4; intuition.
Qed.


Theorem AxD: 
  forall (G: ctx) (A: Formula), G |- #A ==> $A.
Proof.
intros.
assert (T:= AxBoxT G A).
assert (Td:=AxDiaT empty A).
rewrite <- (ctx_conc_empty G).
eapply HK4Dia.trans_dett.
exact T.
exact Td.
Qed.



