Require Import ModalLogic.
Require Import IntuitionisticLogic.
Require Import Slist.
Require Import Context.
Require Export HK4Dia.
Require Import Coq.Program.Equality.

(** Gödel's translation from intuitionistic logic to modal logic *)
Fixpoint godelTrns (A: IFormula) : Formula :=
  match A with
  | IVarp n    => Box (Varp n)
  | IImpl p q  => Box (Impl (godelTrns p) (godelTrns q))
  | IAnd p q   => Box (And (godelTrns p) (godelTrns q))
  | IOr p q    => Box (Or (godelTrns p) (godelTrns q))
  end.

Fixpoint godelCtx (c: Ictx) : ctx :=
  match c with
  | empty => empty
  | snoc G' b  => snoc (godelCtx G') (godelTrns b)
  end.

Proposition gctx_distr:
  forall (G G': Ictx), godelCtx(G;G') = godelCtx G ; godelCtx G'.
Proof.
intros.
induction G'.
- reflexivity.
- simpl.
  rewrite IHG'.
  reflexivity.
Qed.

Lemma elem_gctx: 
  forall (G : Ictx) (A: IFormula),
  elem A G -> elem (godelTrns A) (godelCtx G).
Proof.
intros.
induction G.
- inversion H.
- simpl. inversion H.
  + left. rewrite H0. reflexivity.
  + right. apply IHG. assumption.
Qed.

Lemma empty_box: 
  forall (A: IFormula), empty |- godelTrns A -> empty |- Box (godelTrns A).
Proof.
intros.
induction A; intros; apply Nec; intuition.
Qed.

Lemma trns_box:
  forall (A: IFormula), exists (B: Formula), godelTrns A = Box B.
Proof.
intros.
induction A.
- exists (Varp n).
  reflexivity.
- destruct IHA1 as [B1 IHA1]. destruct IHA2 as [B2 IHA2].
  simpl.
  rewrite IHA1. rewrite IHA2.
  exists (# B1 ==> # B2).
  reflexivity.
- destruct IHA1 as [B1 IHA1]. destruct IHA2 as [B2 IHA2].
  simpl.
  rewrite IHA1. rewrite IHA2.
  exists ((# B1) ∧ (# B2)).
  reflexivity.
- destruct IHA1 as [B1 IHA1]. destruct IHA2 as [B2 IHA2].
  simpl.
  rewrite IHA1. rewrite IHA2.
  exists ((# B1) ∨ (# B2)).
  reflexivity.
Qed.

Lemma gctx_boxed:
  forall (G : Ictx), exists (S: ctx), godelCtx G = boxed S.
Proof.
intro G.
induction G.
- exists empty.
  reflexivity.
- simpl.
  destruct IHG as [ S' IHG].
  rewrite IHG.
  assert (X: exists B, godelTrns x = # B).
  + apply trns_box.
  + destruct X as [ B X'].
    rewrite X'.
    exists (S', B).
    reflexivity.
Qed.

Theorem Int_to_Mod:
  forall (G: Ictx) (A: IFormula), G |-I A -> godelCtx G |- godelTrns A.
Proof.
intros.
dependent induction H.
- apply Hyp.
  apply elem_gctx.
  assumption.
- simpl.
  assert (X: exists S, godelCtx G = boxed S)
  by (apply gctx_boxed).
  destruct X as [ S X'].
  rewrite X'.
  rewrite <- (ctx_conc_empty _).
  apply GenNec.
  apply DeductionTh.
  
  assert (Y: exists A', godelTrns A = # A')
  by (apply trns_box).
  destruct Y as [ A' Y']. rewrite Y'.
  rewrite boxed_snoc.

  rewrite <- (ctx_conc_empty _).
  apply GenNec.
  apply DeductionTh.

  simpl.
  rewrite <- Y'.
  apply inverseDT.
  apply inverseDT.
  apply Ax1.
- simpl.
  assert (X: exists S, godelCtx G = boxed S).
  apply gctx_boxed.
  destruct X as [ S X'].
  rewrite X'.
  rewrite <- (ctx_conc_empty _).
  apply GenNec.

  apply DeductionTh.

  rewrite boxed_snoc.
  rewrite <- (ctx_conc_empty _).
  apply GenNec.
  rewrite <- boxed_snoc.

  apply DeductionTh.

  assert (Y: exists A', godelTrns A = # A')
  by (apply trns_box).
  destruct Y as [ A' Y']. rewrite Y'.
  rewrite boxed_snoc.
  rewrite <- Y'.

  apply ctx_contraction.
  apply (MP _ _ (godelTrns A) _).
  apply Hyp.
  intuition.

  apply ctx_contraction.
  apply (substitution _ _ (# # (godelTrns A ==> godelTrns B)) _).

  apply ctx_contraction.
  apply (MP _ _ (# godelTrns A) _).

  rewrite Y'.
  rewrite boxed_snoc.
  intuition.

  apply AxBoxK_dett.
  intuition.

  apply ctx_contraction.
  apply (substitution _ _ (# (godelTrns A ==> godelTrns B)) _).
  intuition.

  intuition.
- simpl.
  assert (X: exists S, godelCtx G = boxed S).
  apply gctx_boxed.
  destruct X as [ S X'].
  rewrite X'.
  rewrite <- (ctx_conc_empty _).
  apply GenNec.

  apply DeductionTh.
  rewrite boxed_snoc.
  rewrite <- (ctx_conc_empty _).
  apply GenNec.

  apply DeductionTh.

  assert (Y: exists B', godelTrns B = # B')
  by (apply trns_box).
  destruct Y as [ B' Y']. rewrite Y'.
  rewrite boxed_snoc.

  rewrite <- (ctx_conc_empty _).
  apply GenNec.

  apply ctx_contraction.
  apply (MP _ _ (godelTrns B) _).

  simpl.
  rewrite <- Y'.
  intuition.

  simpl.
  rewrite <- Y'.

  apply ctx_contraction.
  apply (MP _ _ ((godelTrns A ==> (godelTrns B ==> godelTrns C))) _ ).

  apply inverseDT.
  apply multihyp_discharge with (n := 1).
  apply trans_dett with (Q := (# (godelTrns B ==> godelTrns C))).

  apply inverseDT.
  apply AxBoxT.

  simpl.
  apply AxBoxT.

  apply Ax3.
- simpl.
  assert (X: exists S, godelCtx G = boxed S).
  apply gctx_boxed.
  destruct X as [ S X'].
  rewrite X'.
  rewrite <- (ctx_conc_empty _).
  apply GenNec.

  apply DeductionTh.
  rewrite boxed_snoc.
  rewrite <- (ctx_conc_empty _).
  apply GenNec.

  apply DeductionTh.
  rewrite boxed_snoc.
  rewrite <- (ctx_conc_empty _).
  apply GenNec.

  apply ctx_contraction.
  apply (MP _ _ (godelTrns A ==> godelTrns B) _).
  simpl.
  intuition.

  apply ctx_contraction.
  apply (MP _ _ (godelTrns B ==> godelTrns C) _).
  simpl.
  intuition.

  apply Ax4.
- simpl.
  assert (X: exists G', godelCtx G = boxed G')
  by (apply gctx_boxed).
  destruct X as [ G' X']. rewrite X'.
  rewrite <- (ctx_conc_empty _).
  apply GenNec.
  apply DeductionTh.

  assert (Y: exists A', godelTrns A = # A').
  apply trns_box.
  destruct Y as [ A' Y'].
  rewrite Y'.
  rewrite boxed_snoc.

  rewrite <- (ctx_conc_empty _).
  apply GenNec.
  apply DeductionTh.

  assert (Z: exists B', godelTrns B = # B').
  apply trns_box.
  destruct Z as [ B' Z'].
  rewrite Z'.
  rewrite boxed_snoc.

  rewrite <- (ctx_conc_empty _).
  apply GenNec.

  simpl.
  rewrite <- Y'.
  rewrite <- Z'.
  apply ctx_contraction.
  apply (MP _ _ (godelTrns B) _).
  intuition.

  apply ctx_contraction.
  apply (MP _ _ (godelTrns A) _).
  intuition.

  apply Ax5.
- simpl.
  assert (X: exists S, godelCtx G = boxed S).
  apply gctx_boxed.
  destruct X as [ S X'].
  rewrite X'.
  rewrite <- (ctx_conc_empty _).
  apply GenNec.
  apply DeductionTh.

  apply ctx_contraction.
  apply (MP _ _ (godelTrns A ∧ godelTrns B) _).

  apply inverseDT.
  apply AxBoxT.

  apply Ax6.
- simpl.
  assert (X: exists S, godelCtx G = boxed S).
  apply gctx_boxed.
  destruct X as [ S X'].
  rewrite X'.
  rewrite <- (ctx_conc_empty _).
  apply GenNec.
  apply DeductionTh.

  apply ctx_contraction.
  apply (MP _ _ (godelTrns A ∧ godelTrns B) _).

  apply inverseDT.
  apply AxBoxT.

  apply Ax7.
- simpl.
  assert (X: exists S, godelCtx G = boxed S).
  apply gctx_boxed.
  destruct X as [ S X'].
  rewrite X'.
  rewrite <- (ctx_conc_empty _).
  apply GenNec.
  apply DeductionTh.

  assert (Y: exists A', godelTrns A = # A').
  apply trns_box.
  destruct Y as [ A' Y'].
  rewrite Y'.
  rewrite boxed_snoc.

  rewrite <- (ctx_conc_empty _).
  apply GenNec.
  simpl.
  rewrite <- Y'.
  apply inverseDT.
  apply Ax8.
- simpl.
  assert (X: exists S, godelCtx G = boxed S).
  apply gctx_boxed.
  destruct X as [ S X'].
  rewrite X'.
  rewrite <- (ctx_conc_empty _).
  apply GenNec.
  apply DeductionTh.

  assert (Y: exists B', godelTrns B = # B').
  apply trns_box.
  destruct Y as [ B' Y'].
  rewrite Y'.
  rewrite boxed_snoc.

  rewrite <- (ctx_conc_empty _).
  apply GenNec.
  simpl.
  rewrite <- Y'.
  apply inverseDT.
  apply Ax9.
- simpl.
  assert (X: exists S, godelCtx G = boxed S).
  apply gctx_boxed.
  destruct X as [ S X'].
  rewrite X'.
  rewrite <- (ctx_conc_empty _).
  apply GenNec.
  apply DeductionTh.
  rewrite boxed_snoc.

  rewrite <- (ctx_conc_empty _).
  apply GenNec.
  apply DeductionTh.
  rewrite boxed_snoc.

  rewrite <- (ctx_conc_empty _).
  apply GenNec.
  apply DeductionTh.
  rewrite boxed_snoc.

  apply ctx_contraction.
  apply (MP _ _ (godelTrns A ∨ godelTrns B) _).

  apply ctx_contraction.
  apply (MP _ _ (# (godelTrns A ∨ godelTrns B)) _).
  intuition.
  apply AxBoxT.

  apply ctx_contraction.
  apply (MP _ _ (godelTrns B ==> godelTrns C) _).
  simpl.

  apply ctx_contraction.
  apply (MP _ _ (# (godelTrns B ==> godelTrns C)) _).
  intuition.
  apply AxBoxT.

  apply ctx_contraction.
  apply (MP _ _ (godelTrns A ==> godelTrns C) _).

  apply ctx_contraction.
  apply (MP _ _ (# (godelTrns A ==> godelTrns C)) _).

  intuition.
  apply AxBoxT.

  apply Ax10.
- rewrite gctx_distr.
  apply ctx_permutation.
  apply (MP _ _ (godelTrns A) _).
  exact IHDerivI1.

  simpl in IHDerivI2.
  apply ctx_contraction.
  apply (MP _ _ (# (godelTrns A ==> godelTrns B)) _).
  exact IHDerivI2.
  apply AxBoxT.
Qed.
