(** GS4 from 'On Interactive Proof-Search for ConstructiveModal Necessity' *)

Require Import Coq.Program.Equality.
Require Import ModalLogic.
Require Import LabeledSequents.
Require Import Slist.
Require Import Context.
Require Import List.
(* Require Import Permutations.*)

(** --------------- INFERENCE RULES --------------- *)
(** The distinction between valid and true hypotheses is expressed by 
    separate sets of hypotheses, ie two contexts as arguments for the
    hypothetical judments.
    This definition incorporates the possibility judgment in 
    dedicated rules with explicit constructors. *)
Inductive DC_Proof : LCtx -> LCtx -> Formula -> Prop :=

| dc_thyp : forall (D: LCtx) (G G': LCtx) (n : nat) (A: Formula),
             DC_Proof D (G, (n -: A); G') A

| dc_vhyp : forall (D D': LCtx) (G: LCtx) (n : nat) (A: Formula),
             DC_Proof (D, (n -: A); D') G A

| dc_impR : forall (D: LCtx) (G: LCtx) (n : nat) (A B: Formula),
             DC_Proof D (G, (n -: A)) B -> DC_Proof D G (A ==> B)

| dc_conjR : forall (D: LCtx) (G: LCtx) (A B: Formula),
             DC_Proof D G A ->
             DC_Proof D G B -> DC_Proof D G (A ∧ B)

| dc_disjR_L : forall (D: LCtx) (G: LCtx) (A B: Formula),
             DC_Proof D G A -> DC_Proof D G (A ∨ B)

| dc_disjR_R : forall (D: LCtx) (G: LCtx) (A B: Formula),
             DC_Proof D G B -> DC_Proof D G (A ∨ B)

| dc_boxR : forall (D: LCtx) (G: LCtx) (A: Formula),
             DC_Proof D empty A -> DC_Proof D G (# A)

| dc_diaR : forall (D: LCtx) (G: LCtx) (A: Formula),
            DC_Proof D G A -> DC_Proof D G ($ A)

| dc_disjL : forall (D: LCtx) (G G': LCtx) (n m j : nat) (A B C: Formula),
             DC_Proof D (G, (n -: A); G') C ->
             DC_Proof D (G, (m -: B); G') C ->
             DC_Proof D (G, (j -: (A ∨ B)); G') C

| dc_conjL : forall (D: LCtx) (G G': LCtx) (n m j: nat) (A B C : Formula),
             DC_Proof D ((G, (n -: A), (m -: B)); G') C -> 
             DC_Proof D ((G, (j -: (A ∧ B)));G') C

| dc_impL : forall (D: LCtx) (G G': LCtx) (n m : nat) (A B: Formula),
            DC_Proof D (G, (n -: (A ==> B)); G') A ->
            DC_Proof D (G, (m -: (A ==> B)); G') B

| dc_boxL : forall (D: LCtx) (G G': LCtx) (n m: nat) (A B : Formula),
            DC_Proof (D, (n -: A)) (G; G') B ->
            DC_Proof D ((G, (m -: #A));G') B

| dc_diaL: forall (D: LCtx) (G G': LCtx) (n m : nat) (A C :Formula),
            DC_Proof D (empty, (n -: A)) ($ C) ->
            DC_Proof D (G, (m -: $A); G') ($ C)

| dc_disjLV : forall (D D': LCtx) (G: LCtx) (n m : nat) (A B C: Formula),
            DC_Proof (D, (n -: (A ∨ B)); D') (G, (m -: A)) C ->
            DC_Proof (D, (n -: (A ∨ B)); D') (G, (m -: B)) C ->
            DC_Proof (D, (n -: (A ∨ B)); D') G C

| dc_conjLV : forall (D D': LCtx) (G: LCtx) (n m j : nat) (A B C: Formula),
            DC_Proof ((D, (n -: A), (m -: B)); D') G C ->
            DC_Proof ((D, (j -: (A ∧ B)));D') G C

| dc_impLV : forall (D D': LCtx) (G: LCtx) (n : nat) (A B: Formula),
             DC_Proof (D, (n -: (A ==> B)); D') G A ->
             DC_Proof (D, (n -: (A ==> B)); D') G B

| dc_boxLV : forall (D D': LCtx) (G: LCtx) (n : nat) (A B : Formula),
            DC_Proof (D, (n -: A); D') G B ->
            DC_Proof (D, (n -: #A); D') G B

| dc_diaLV: forall (D D' : LCtx) (G : LCtx) (n m : nat) (A C : Formula),
            DC_Proof (D, (n -: $A);D') (empty, (m -: A)) ($C) ->
            DC_Proof (D, (n -: $A);D') G ($C)

| dc_cut : forall (D D': LCtx) (G G': LCtx) (n : nat) (A B : Formula),
            DC_Proof D G A ->
            DC_Proof D' (G', (n -: A)) B ->
            DC_Proof (D;D') (G;G') B

| dc_cutV : forall (D: LCtx) (G: LCtx) (n : nat) (A B : Formula),
            DC_Proof D empty A ->
            DC_Proof (D, (n -: A)) G B ->
            DC_Proof D G B.

Global Hint Constructors DC_Proof : GS4.

Notation "D ∥ G ⊢s A" := (DC_Proof D G A) (at level 30).

Notation "[ x ; .. ; y ]" := (cons x .. (cons y nil) ..).

Definition newHyp (G : LCtx) (A : Formula) : LCtx :=
  match G with
  | empty => empty, (pair 0 A)
  | G', p => let 'pair n B := p in G', (pair n B), (pair (S n) A)
  end.

Fixpoint moveLabelsPlus (G : LCtx) : LCtx :=
  match G with
  | empty => empty
  | G', p => let 'pair n A := p in moveLabelsPlus G', (pair (S n) A)
  end.

Fixpoint moveLabelsMinus (G : LCtx) : LCtx :=
  match G with
  | empty => empty
  | G', p => let 'pair n A := p in moveLabelsMinus G', ((n - 1) -: A)
  end. 

Inductive Tactics : LSeq -> LSeq -> Prop:=

  | tassumption: forall (D G G': LCtx) (n : nat) (A : Formula),
    Tactics [ DC_Proof D (G, (n -: A); G') A ]
            nil

  | tvassumption: forall (D D' G : LCtx) (n : nat) (A : Formula),
    Tactics [ DC_Proof (D, (n -: A); D') G A ]
            nil

  | tintro: forall (D G : LCtx) (A B : Formula),
    Tactics [ DC_Proof D G (A ==> B) ]
            [ DC_Proof D (newHyp G A) B ]

  | tsplit: forall (D G : LCtx) (A B : Formula),
    Tactics [ DC_Proof D G (A ∧ B) ]
            ( [DC_Proof D G A] ++ [DC_Proof D G B] )

  | tleft: forall (D G : LCtx) (A B : Formula),
    Tactics [ DC_Proof D G (A ∨ B) ]
            [ DC_Proof D G A ]

  | tright: forall (D G : LCtx) (A B : Formula),
    Tactics [ DC_Proof D G (A ∨ B) ]
            [ DC_Proof D G B ]

  | tneccesitation: forall (D G : LCtx) (A : Formula),
    Tactics [ DC_Proof D G (# A) ]
            [ DC_Proof D empty A ]

  | tdia : forall (D G : LCtx) (A : Formula),
    Tactics [ DC_Proof D G ($ A) ]
            [ DC_Proof D G A ]

  | tdestructdisj: forall (D G G' : LCtx) (n : nat) (A B C: Formula),
    Tactics [ DC_Proof D (G, (n -: (A ∨ B)); G') C ]
            ( [DC_Proof D (G, (n -: A); G') C]
           ++ [DC_Proof D (G, (n -: B); G') C] )

  | tdestructconj: forall (D G G' : LCtx) (n : nat) (A B C: Formula),
    Tactics [ DC_Proof D (G, (n -: (A ∧ B)); G') C ]
            [ DC_Proof D (G, (n -: A); moveLabelsPlus (empty, (n -: B); G')) C ]

  | tapply: forall (D G G' : LCtx) (n : nat) (A B : Formula),
    Tactics [ DC_Proof D (G, (n -: (A ==> B)); G') B ]
            [ DC_Proof D (G, (n -: (A ==> B)); G') A ]

  | tdestructbox: forall (D G G' : LCtx) (n : nat) (A B: Formula),
    Tactics [ DC_Proof D (G, (n -: (# A)); G') B ]
            [ DC_Proof (newHyp D A) (G ; moveLabelsMinus G') B]

  | tdial: forall (D G G' : LCtx) (n : nat) (A C: Formula),
    Tactics [ DC_Proof D (G, (n -: ($ A)); G') ($ C) ]
            [ DC_Proof D (empty, (n -: ($ A))) ($ C) ]

  | tvdestructdisj: forall (D D' G : LCtx) (n : nat) (A B C: Formula),
    Tactics [ DC_Proof (D, (n -: (A ∨ B)); D') G C ]
            ( [DC_Proof (D; moveLabelsMinus D') (newHyp G A) C]
           ++ [DC_Proof (D; moveLabelsMinus D') (newHyp G B) C] )

  | tvdestructconj: forall (D D' G : LCtx) (n : nat) (A B C: Formula),
    Tactics [ DC_Proof (D, (n -: (A ∧ B)); D') G C ]
            [ DC_Proof (D, (n -: A); moveLabelsPlus (empty, (n -: B); D')) G C ]

  | tvapply: forall (D D' G : LCtx) (n : nat) (A B : Formula),
    Tactics [ DC_Proof (D, (n -: (A ==> B)); D') G B ]
            [ DC_Proof (D, (n -: (A ==> B)); D') G A ]

  | tvdestructbox: forall (D D' G : LCtx) (n : nat) (A B: Formula),
    Tactics [ DC_Proof (D, (n -: (# A)); D') G B ]
            [ DC_Proof (D, (n -: A); D') G B ]

  | tdiaLV: forall (D D' G : LCtx) (n m : nat) (A C: Formula),
    Tactics [ DC_Proof (D, (n -: ($A)); D') G ($ C) ]
            [ DC_Proof (D, (n -: ($A)); D') (empty, (m -: A)) ($ C) ]

  | tcut: forall (D G : LCtx) (A B: Formula),
    Tactics [ DC_Proof D G B ]
            ( [DC_Proof D (newHyp G A) B] ++ [DC_Proof D G A] )

  | tvcut: forall (D G : LCtx) (A B: Formula),
    Tactics [ DC_Proof D G B ]
            ( [DC_Proof (newHyp D A) G B] ++ [DC_Proof D empty A] )

  | tassert: forall (D G : LCtx) (A C: Formula),
    Tactics [ DC_Proof D G C ]
            ( [DC_Proof D G A] ++ [DC_Proof D (newHyp G A) C] )

  | tvassert: forall (D G : LCtx) (A C: Formula),
    Tactics [ DC_Proof D G C ]
            ( [DC_Proof D empty A] ++ [DC_Proof (newHyp D A) G C] )

  | seq: forall (S1 S2 S : LSeq),
    Tactics S1 S2 -> Tactics (S1 ++ S) (S2 ++ S).

Global Hint Constructors Tactics : GS4.

Notation "S ▷ S'" := (Tactics S S') (at level 30).

Inductive TacticsCls : LSeq -> LSeq -> Prop :=

  | tc1: forall (S S' : LSeq),
    S ▷ S' -> TacticsCls S S'

  | tc2: forall (S S' S'' : LSeq),
    S ▷ S' -> TacticsCls S' S'' -> TacticsCls S S''.

Notation "S ▷+ S'" := (TacticsCls S S') (at level 30).

(* Lemma 4.2 *)
Lemma SeqPlus:
  forall (S S1 S2 : LSeq),
  S1 ▷+ S2 -> (S1 ++ S) ▷+ (S2 ++ S).
Proof.
intros.
dependent induction H.
+ apply tc1.
  apply seq.
  assumption.
+ eapply tc2.
  apply seq.
  exact H.
  assumption.
Qed.



(* Lemma 4.4 *)
Lemma SeqSolv:
  forall (S1 S2 : LSeq),
  S1 ▷ S2 -> S2 ▷+ nil -> S1 ▷+ nil.
Proof.
intros.
dependent induction H.
- apply tc1.
  apply tassumption.
- apply tc1.
  apply tvassumption.
- eapply tc2.
  apply tintro.
  assumption.
- eapply tc2.
  apply tsplit.
  assumption.
- eapply tc2.
  apply tleft.
  assumption.
- eapply tc2.
  apply tright.
  assumption.
- eapply tc2.
  apply tneccesitation.
  assumption.
- eapply tc2.
  apply tdia.
  assumption.
- eapply tc2.
  apply tdestructdisj.
  assumption.
- eapply tc2.
  apply tdestructconj.
  assumption.
- eapply tc2.
  apply tapply.
  assumption.
- eapply tc2.
  apply tdestructbox.
  assumption.
- eapply tc2.
  apply tdial.
  assumption.
- eapply tc2.
  apply tvdestructdisj.
  assumption.
- eapply tc2.
  apply tvdestructconj.
  assumption.
- eapply tc2.
  apply tvapply.
  assumption.
- eapply tc2.
  apply tvdestructbox.
  assumption.
- apply (tc2 _ [(D, (n -: ($ A))); D' ∥ empty, (m -: A) |-s ($ C)] _).
  apply tdiaLV.
  assumption.
- apply (tc2 _ ([D ∥ newHyp G A |-s B] ++ [D ∥ G |-s A]) _).
  apply tcut.
  assumption.
- apply (tc2 _ ([newHyp D A ∥ G |-s B] ++ [D ∥ empty |-s A]) _).
  apply tvcut.
  assumption.
- apply (tc2 _ ([D ∥ G |-s A] ++ [D ∥ newHyp G A |-s C]) _).
  apply tassert.
  assumption.
- apply (tc2 _ ([D ∥ empty |-s A] ++ [newHyp D A ∥ G |-s C]) _).
  apply tvassert.
  assumption.
- eapply tc2.
  apply seq.
  exact H.
  assumption.
Qed.

(* Lemma 4.5 *)
Lemma SeqSolvPlus:
  forall (S1 S2 : LSeq),
  S1 ▷+ S2 -> S2 ▷+ nil -> S1 ▷+ nil.
Proof.
intros.
dependent induction H.
eapply SeqSolv.
exact H.
exact H0.
eapply tc2.
exact H.
apply IHTacticsCls.
exact H1.
Qed.

(* Theorem equiv1:
  forall (D G : LCtx) (A : Formula),
  [DC_Proof D G A] ▷+ nil -> DC_Proof D G A.
Proof. *)


Lemma test:
  forall (A : Formula),
  [DC_Proof empty (empty, (0 -: A)) A] ▷+
  nil.
Proof.
intros.
apply tc1.
apply (tassumption empty empty empty 0 A).
Qed.

Lemma test2:
  forall (A B : Formula),
  [DC_Proof empty (empty, (0 -: A)) (A ∨ B)] ▷+
  nil.
Proof.
intros.
apply (tc2 _ [DC_Proof empty (empty, (0 -: A)) A] _).
apply tleft.
apply tc1.
apply (tassumption empty empty empty 0 A).
Qed.

(* Ltac solve_axioms :=
match goal with
  | |- [DC_Proof ?D (?G, (?n -: ?A), ?G') ?A] ▷ nil =>
        eapply (tassumption _ _ G' _ _)
end. *)

Lemma test3:
  forall (A B : Formula),
  [DC_Proof empty (empty, (0 -: A), (1 -: B)) (A ∧ B)] ▷+
  nil.
Proof.
intros.
apply (tc2 _ (
[DC_Proof empty (empty, (0 -: A), (1 -: B)) A] ++
[DC_Proof empty (empty, (0 -: A), (1 -: B)) B]) _).
apply tsplit.

apply (tc2 
([DC_Proof empty (empty, (0 -: A), (1 -: B)) A] ++
[DC_Proof empty (empty, (0 -: A), (1 -: B)) B])

[DC_Proof empty (empty, (0 -: A), (1 -: B)) B] nil).

apply (seq
[DC_Proof empty ((empty, (0 -: A)), (1 -: B)) A] 
nil
[DC_Proof empty ((empty, (0 -: A)), (1 -: B)) B]).

apply (tassumption empty empty (empty, (1 -: B)) 0 A).

apply tc1.
apply (tassumption empty (empty, (0 -: A)) empty 1 B).
Qed.

Lemma test4:
  forall (A B : Formula),
  [DC_Proof empty (empty, (0 -: A), (1 -: B)) (A ∧ B)] ▷+
  nil.
Proof.
intros.
eapply tc2.
apply tsplit.
eapply tc2.
apply seq.
eapply (tassumption _ _ (empty, (1 -: B)) _ _).
apply (tassumption empty empty (empty, (1 -: B)) 0 A).
apply tc1.
apply (tassumption empty (empty, (0 -: A)) empty 1 B).
Qed.

(* Lemma test3:
  forall (A B : Formula),
  [DC_Proof empty (empty, (0 : A), (1 : B)) (A ∧ B)] ▷+
  nil.
Proof.
intros.
(* apply (tc2 _ ([DC_Proof empty (empty, (0 : A), (1 : B)) A] ++
[DC_Proof empty (empty, (0 : A), (1 : B)) B]) _). *)
apply (tc2 _ [DC_Proof empty (empty, (0 : A), (1 : B)) A] _).
admit.
apply tc1.
apply (tassumption empty empty (empty, (1 : B)) 0 A). *)

(* Lemma test:
  forall (A B : Formula),
  [DC_Proof empty (empty, (0 : A), (1 : B)) (A ∧ B)] ▷+
  nil.
Proof.
intros.
apply (tc2 _ ([DC_Proof empty (empty, (0 : A), (1 : B)) A] ++
[DC_Proof empty (empty, (0 : A), (1 : B)) B]) _).
apply tsplit.
apply tc1.

apply seq.
apply (seq).
apply tassumption. *)

