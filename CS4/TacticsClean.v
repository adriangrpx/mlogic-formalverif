(** GS4 from 'On Interactive Proof-Search for ConstructiveModal Necessity' *)

Require Import Coq.Program.Equality.
Require Import Coq.Arith.EqNat.
Require Import Coq.Init.Nat.
Require Import ModalLogic.
Require Import List.

Definition LSeq : Type := list Prop.

Inductive id : Type :=
  | Id : nat -> id.

Definition beq_id (a b: id) :=
  match a with
    | Id n => 
       match b with
        | Id m => beq_nat n m
       end
  end.

Theorem beq_id_refl : forall id, true = beq_id id id.
Proof.
  intros [n]. 
  simpl.
  rewrite <- beq_nat_refl.
  reflexivity.
Qed.

Theorem beq_id_true_iff : forall (id1 id2 : id),
  beq_id id1 id2 = true <-> id1 = id2.
Proof.
   intros [n1] [n2].
   unfold beq_id.
   rewrite beq_nat_true_iff.
   split.
   - (* -> *) intros H. rewrite H. reflexivity.
   - (* <- *) intros H. inversion H. reflexivity.
Qed.

Definition total_map (A:Type) := id -> A.

Definition t_empty {A:Type} (v : A) : total_map A :=
  (fun _ => v).

Definition t_update {A:Type} (m : total_map A)
                    (x : id) (v : A) :=
  fun x' => if beq_id x x' then v else m x'.

Notation "'_' '!->' v" := (t_empty v)
  (at level 100, right associativity).

Notation "x '!->' v '&' m" := (t_update m x v)
  (at level 100, v at next level, right associativity).

Definition partial_map (A : Type) := total_map (option A).
Definition empty_map {A : Type} : partial_map A :=
  t_empty None.

Definition update {A : Type} (m : partial_map A)
           (x : id) (v : A)  :=
   (x !-> Some v & m).

Notation "x '⊢>' v '&' m" := (update m x v)
  (at level 100, v at next level, right associativity).

Notation "x '⊢>' v" := (update empty_map x v)
  (at level 100).

Definition context : Type := (partial_map Formula * (list nat))%type.

Definition empty_ctx : context := pair empty_map nil.

Definition diff (l1 l2 : list nat) : list nat :=
  List.filter (fun n => negb (List.existsb (Nat.eqb n) l2)) l1.

Definition min_index (G : context): nat :=
 let m := fst G in
 let ml := snd G in
 let mllen := (length ml) in
 let l  := seq 0 mllen in
 match diff l ml with
  | nil => mllen
  | n :: _ => n
 end.

Definition new_hypo (G : context) (v : Formula) : context :=
 let i := min_index G in
 let m := fst G in
 let l := snd G in
 (pair (Id i !-> Some v & m) (i :: l)).

(** --------------- INFERENCE RULES --------------- *)
(** The distinction between valid and true hypotheses is expressed by 
    separate sets of hypotheses, ie two contexts as arguments for the
    hypothetical judments.
    This definition incorporates the possibility judgment in 
    dedicated rules with explicit constructors. *)
Inductive DC_Proof : context -> context -> Formula -> Prop :=

| dc_thyp : forall (D: context) (G: context) (A: Formula),
             DC_Proof D (new_hypo G A) A

| dc_vhyp : forall (D: context) (G: context) (A: Formula),
             DC_Proof (new_hypo D A) G A

| dc_impR : forall (D: context) (G: context) (A B: Formula),
             DC_Proof D (new_hypo G A) B -> DC_Proof D G (A ==> B)

| dc_conjR : forall (D: context) (G: context) (A B: Formula),
             DC_Proof D G A ->
             DC_Proof D G B -> DC_Proof D G (A ∧ B)

| dc_disjR_L : forall (D: context) (G: context) (A B: Formula),
             DC_Proof D G A -> DC_Proof D G (A ∨ B)

| dc_disjR_R : forall (D: context) (G: context) (A B: Formula),
             DC_Proof D G B -> DC_Proof D G (A ∨ B)

| dc_boxR : forall (D: context) (G: context) (A: Formula),
             DC_Proof D empty_ctx A -> DC_Proof D G (# A)

| dc_diaR : forall (D: context) (G: context) (A: Formula),
            DC_Proof D G A -> DC_Proof D G ($ A)

| dc_disjL : forall (D: context) (G : context) (A B C: Formula),
             DC_Proof D (new_hypo G A) C ->
             DC_Proof D (new_hypo G B) C ->
             DC_Proof D (new_hypo G (A ∨ B)) C

| dc_conjL : forall (D: context) (G : context) (A B C : Formula),
             DC_Proof D (new_hypo (new_hypo G A) B) C -> 
             DC_Proof D (new_hypo G (A ∧ B)) C

| dc_impL : forall (D: context) (G G': context) (A B: Formula),
            DC_Proof D (new_hypo G (A ==> B)) A ->
            DC_Proof D (new_hypo G (A ==> B)) B

| dc_boxL : forall (D: context) (G: context) (A B : Formula),
            DC_Proof (new_hypo D A) G B ->
            DC_Proof D (new_hypo G (# A)) B

| dc_diaL: forall (D: context) (G: context) (A C : Formula),
            DC_Proof D (new_hypo empty_ctx A) ($ C) ->
            DC_Proof D (new_hypo G ($ A)) ($ C)

| dc_disjLV : forall (D: context) (G: context) (A B C: Formula),
            DC_Proof (new_hypo D (A ∨ B)) (new_hypo G A) C ->
            DC_Proof (new_hypo D (A ∨ B)) (new_hypo G B) C ->
            DC_Proof (new_hypo D (A ∨ B)) G C

| dc_conjLV : forall (D: context) (G: context) (A B C: Formula),
            DC_Proof (new_hypo (new_hypo D A) B) G C ->
            DC_Proof (new_hypo D (A ∧ B)) G C

| dc_impLV : forall (D: context) (G: context) (A B: Formula),
             DC_Proof (new_hypo D (A ==> B)) G A ->
             DC_Proof (new_hypo D (A ==> B)) G B

| dc_boxLV : forall (D: context) (G: context) (A B : Formula),
            DC_Proof (new_hypo D A) G B ->
            DC_Proof (new_hypo D (# A)) G B

| dc_diaLV: forall (D: context) (G : context) (A C : Formula),
            DC_Proof (new_hypo D ($ A)) (new_hypo empty_ctx A) ($ C) ->
            DC_Proof (new_hypo D ($ A)) G ($ C)

| dc_cut : forall (D : context) (G : context) (A B : Formula),
            DC_Proof D G A ->
            DC_Proof D (new_hypo G A) B ->
            DC_Proof D G B

| dc_cutV : forall (D: context) (G: context) (A B : Formula),
            DC_Proof D empty_ctx A ->
            DC_Proof (new_hypo D A) G B ->
            DC_Proof D G B.

Global Hint Constructors DC_Proof : GS4.

Notation "D ∥ G ⊢s A" := (DC_Proof D G A) (at level 30).

Definition SeqT : Type := (context * (context * Formula))%type.

Notation "D ∥ G ⊢ A" := (pair D (pair G A)) (at level 20).

(* Definition 3.2 *)
Definition LSeqT : Type := list SeqT.

Definition solvableT (S : SeqT) : Prop :=
 match S with
  | pair D p => match p with
    | pair G A => DC_Proof D G A
    end
  end.

Fixpoint solvableLSeqT (S : LSeqT) : Prop :=
 match S with
   | nil => True
   | s :: S' => solvableT s /\ solvableLSeqT S'
 end.

Lemma LSeqTConcNil: forall (S : LSeqT),
 S ++ nil = S.
Proof.
intros.
intuition.
Qed.

Lemma solvableLSeqTConc: forall (S1 S : LSeqT),
  solvableLSeqT (S1 ++ S) <-> solvableLSeqT S1 /\ solvableLSeqT S.
Proof.
intros.
induction S1; simpl in *; intuition.
Qed.

Notation "[ x ; .. ; y ]" := (cons x .. (cons y nil) ..).

Inductive Tactics : LSeqT -> LSeqT -> Prop :=

  | tassumption: forall (D G: context) (A : Formula),
    Tactics [ D ∥ (new_hypo G A) ⊢ A ]
            nil

  | tvassumption: forall (D G : context) (A : Formula),
    Tactics [ (new_hypo D A) ∥ G ⊢ A ]
            nil

  | tintro: forall (D G : context) (A B : Formula),
    Tactics [ D ∥ G ⊢ (A ==> B) ]
            [ D ∥ (new_hypo G A) ⊢ B ]

  | tsplit: forall (D G : context) (A B : Formula),
    Tactics [ D ∥ G ⊢ (A ∧ B) ]
            ( [D ∥ G ⊢ A] ++ [D ∥ G ⊢ B] )

  | tleft: forall (D G : context) (A B : Formula),
    Tactics [ D ∥ G ⊢(A ∨ B) ]
            [ D ∥ G ⊢ A ]

  | tright: forall (D G : context) (A B : Formula),
    Tactics [ D ∥ G ⊢ (A ∨ B) ]
            [ D ∥ G ⊢ B ]

  | tneccesitation: forall (D G : context) (A : Formula),
    Tactics [ D ∥ G ⊢ (# A) ]
            [ D ∥ empty_ctx ⊢ A ]

  | tdia : forall (D G : context) (A : Formula),
    Tactics [ D ∥ G ⊢ ($ A) ]
            [ D ∥ G ⊢ A ]

  | tdestructdisj: forall (D G : context) (A B C: Formula),
    Tactics [ D ∥ (new_hypo G (A ∨ B)) ⊢ C ]
            ( [ D ∥ (new_hypo G A) ⊢ C]
           ++ [ D ∥ (new_hypo G B) ⊢ C] )

  | tdestructconj: forall (D G : context) (A B C: Formula),
    Tactics [ D ∥ (new_hypo G (A ∧ B)) ⊢ C ]
            [ D ∥ (new_hypo (new_hypo G A) B) ⊢ C ]

  | tapply: forall (D G : context) (A B : Formula),
    Tactics [ D ∥ (new_hypo G (A ==> B)) ⊢ B ]
            [ D ∥ (new_hypo G (A ==> B)) ⊢ A ]

  | tdestructbox: forall (D G : context) (A B: Formula),
    Tactics [ D ∥ (new_hypo G (# A)) ⊢ B ]
            [ (new_hypo D A) ∥ G ⊢ B]

  | tdial: forall (D G : context) (A C: Formula),
    Tactics [ D ∥ (new_hypo G ($ A)) ⊢ ($ C) ]
            [ D ∥ (new_hypo empty_ctx A) ⊢ ($ C) ]

  | tvdestructdisj: forall (D G : context) (A B C: Formula),
    Tactics [   (new_hypo D (A ∨ B)) ∥ G ⊢ C ]
            ( [ (new_hypo D (A ∨ B)) ∥ (new_hypo G A) ⊢ C]
           ++ [ (new_hypo D (A ∨ B)) ∥ (new_hypo G B) ⊢ C] )

  | tvdestructconj: forall (D G : context) (A B C: Formula),
    Tactics [ new_hypo D (A ∧ B) ∥ G ⊢ C ]
            [ new_hypo (new_hypo D A) B ∥ G ⊢ C ]

  | tvapply: forall (D G : context) (A B : Formula),
    Tactics [ new_hypo D (A ==> B) ∥ G ⊢ B ]
            [ new_hypo D (A ==> B) ∥ G ⊢ A ]

  | tvdestructbox: forall (D G : context) (A B: Formula),
    Tactics [ new_hypo D (# A) ∥ G ⊢ B ]
            [ new_hypo D A ∥ G ⊢ B ]

  | tdiaLV: forall (D G : context) (A C: Formula),
    Tactics [ new_hypo D ($ A) ∥ G ⊢ ($ C) ]
            [ new_hypo D ($ A) ∥ new_hypo empty_ctx A ⊢ ($ C) ]

  | tcut: forall (D G : context) (A B: Formula),
    Tactics [ D ∥ G ⊢ B ]
            ( [ D ∥ (new_hypo G A) ⊢ B ] ++ [ D ∥ G ⊢ A ] )

  | tvcut: forall (D G : context) (A B: Formula),
    Tactics [ D ∥ G ⊢ B ]
            ( [ (new_hypo D A) ∥ G ⊢ B] ++ [ D ∥ empty_ctx ⊢ A] )

  | tassert: forall (D G : context) (A C: Formula),
    Tactics [ D ∥ G ⊢ C ]
            ( [ D ∥ G ⊢ A ] ++ [ D ∥ (new_hypo G A) ⊢ C] )

  | tvassert: forall (D G : context) (A C: Formula),
    Tactics [ D ∥ G ⊢ C ]
            ( [ D ∥ empty_ctx ⊢ A] ++ [ (new_hypo D A) ∥ G ⊢ C] )

  | seq: forall (S1 S2 S : LSeqT),
    Tactics S1 S2 -> Tactics (S1 ++ S) (S2 ++ S).

Global Hint Constructors Tactics : GS4.

Notation "S ▷ S'" := (Tactics S S') (at level 30).

Inductive TacticsCls : LSeqT -> LSeqT -> Prop :=

  | tc1: forall (S S' : LSeqT),
    S ▷ S' -> TacticsCls S S'

  | tc2: forall (S S' S'' : LSeqT),
    S ▷ S' -> TacticsCls S' S'' -> TacticsCls S S''.

Notation "S ▷+ S'" := (TacticsCls S S') (at level 30).

(* Lemma 4.2 *)
Lemma SeqPlus:
  forall (S S1 S2 : LSeqT),
  S1 ▷+ S2 -> (S1 ++ S) ▷+ (S2 ++ S).
Proof.
intros.
dependent induction H.
+ apply tc1.
  apply seq.
  assumption.
+ eapply tc2.
  apply seq.
  exact H.
  assumption.
Qed.

(* Lemma 4.4 *)
Lemma SeqSolv:
  forall (S1 S2 : LSeqT),
  S1 ▷ S2 -> solvableLSeqT S2 -> solvableLSeqT S1.
Proof.
intros.
dependent induction H.
1-19: simpl in *; split; intuition.
- eapply dc_cut.
  exact H0.
  exact H.
- simpl in *; split; intuition.
  eapply dc_cutV.
  exact H0.
  exact H.
- simpl in *. split; intuition.
  eapply dc_cut.
  exact H.
  exact H0.
- simpl in *; split; intuition.
  eapply dc_cutV.
  exact H.
  exact H0.
- apply solvableLSeqTConc.
  apply solvableLSeqTConc in H0.
  intuition.
Qed.

(* Lemma 4.5 *)
Lemma SeqSolvPlus:
  forall (S1 S2 : LSeqT),
  S1 ▷+ S2 -> solvableLSeqT S2 -> solvableLSeqT S1.
Proof.
intros.
dependent induction H.
- apply SeqSolv in H.
  exact H.
  exact H0.
- apply SeqSolv in H.
  exact H.
  apply IHTacticsCls.
  exact H1.
Qed.

Lemma concatNil:
  forall (S1 S2 : LSeqT),
  S1 ▷+ nil -> S2 ▷+ nil -> (S1 ++ S2) ▷+ nil.
Proof.
intros.
dependent induction H.
- apply seq with (S := S2) in H.
  simpl in H.
  eapply tc2.
  exact H.
  exact H0.
- intuition.
  apply seq with (S := S2) in H.
  eapply tc2.
  exact H.
  exact H3.
Qed.

Lemma derivableSolvable:
  forall (D G : context) (A : Formula),
  DC_Proof D G A <-> solvableLSeqT [D ∥ G ⊢ A].
Proof.
intros.
split.
- intros.
  unfold solvableLSeqT.
  intuition.
- intros.
  unfold solvableLSeqT in H.
  intuition.
Qed.

(* Theorem 4.6 *)
Theorem forwardBackwardEquivs:
  forall (S : SeqT),
  match S with
  | pair D (pair G A) => DC_Proof D G A <-> [S] ▷+ nil
  end.
Proof.
intros.
destruct S as [D].
destruct p as [G A].
split.
- intros.
  dependent induction H.
  + apply tc1.
    apply tassumption.
  + apply tc1.
    apply tvassumption.
  + eapply tc2.
    apply tintro.
    exact IHDC_Proof.
  + eapply tc2.
    apply tsplit.
    apply concatNil.
    exact IHDC_Proof1.
    exact IHDC_Proof2.
  + eapply tc2.
    apply tleft.
    exact IHDC_Proof.
  + eapply tc2.
    apply tright.
    exact IHDC_Proof.
  + eapply tc2.
    apply tneccesitation.
    exact IHDC_Proof.
  + eapply tc2.
    apply tdia.
    exact IHDC_Proof.
  + eapply tc2.
    apply tdestructdisj.
    apply concatNil.
    exact IHDC_Proof1.
    exact IHDC_Proof2.
  + eapply tc2.
    apply tdestructconj.
    exact IHDC_Proof.
  + eapply tc2.
    apply tapply.
    exact IHDC_Proof.
  + eapply tc2.
    eapply tdestructbox.
    exact IHDC_Proof.
  + eapply tc2.
    apply tdial.
    exact IHDC_Proof.
  + eapply tc2.
    apply tvdestructdisj.
    apply concatNil.
    exact IHDC_Proof1.
    exact IHDC_Proof2.
  + eapply tc2.
    apply tvdestructconj.
    exact IHDC_Proof.
  + eapply tc2.
    apply tvapply.
    exact IHDC_Proof.
  + eapply tc2.
    apply tvdestructbox.
    exact IHDC_Proof.
  + eapply tc2.
    apply tdiaLV.
    exact IHDC_Proof.
  + eapply tc2.
    apply tassert.
    apply concatNil.
    exact IHDC_Proof1.
    exact IHDC_Proof2.
  + eapply tc2.
    apply tvassert.
    apply concatNil.
    exact IHDC_Proof1.
    exact IHDC_Proof2.
- intros.
  apply SeqSolvPlus in H.
  unfold solvableLSeqT in H.
  intuition.
  simpl.
  intuition.
Qed.