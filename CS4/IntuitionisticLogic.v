(**
 Intuitionistic Logic
 Language: propositional logic with implication, conjuction and disyunction.
 *)

Require Import Coq.Strings.String.
Require Import Arith.
Require Import Bool.
Require Import Slist.

Open Scope nat_scope.

Set Implicit Arguments.


(** --------------- SYNTAX --------------- *)

Inductive IFormula : Type :=
  | IVarp : nat -> IFormula
  | IImpl : IFormula -> IFormula -> IFormula
  | IAnd  : IFormula -> IFormula -> IFormula
  | IOr   : IFormula -> IFormula -> IFormula.

Notation "x ==>I y"  := (IImpl x y) (at level 55, right associativity).
Notation "x ∧I y" := (IAnd x y) (at level 53, right associativity).
Notation "x ∨I y" := (IOr x y) (at level 53, right associativity).

Definition Ictx := slist IFormula.

Inductive DerivI: Ictx -> IFormula -> Prop:=
| Hyp:    forall (G: Ictx) (A: IFormula),
          elem A G -> DerivI G A
      
| Ax1:    forall G A B,
          DerivI G (A ==>I (B ==>I A))
       
| Ax2:    forall G A B,
          DerivI G ((A ==>I (A ==>I B)) ==>I (A ==>I B))
       
| Ax3:    forall (G: Ictx) (A B C: IFormula), 
          DerivI G ((A ==>I (B ==>I C)) ==>I (B ==>I (A ==>I C)))
       
| Ax4:    forall (G: Ictx) (A B C: IFormula), 
          DerivI G ((B ==>I C) ==>I ((A ==>I B) ==>I (A ==>I C)))
       
| Ax5:  forall G A B,
        DerivI G (A ==>I B ==>I (A ∧I B))
       
| Ax6:  forall G A B,
        DerivI G (A ∧I B ==>I A)
       
| Ax7:  forall G A B,
        DerivI G (A ∧I B ==>I B)
       
| Ax8:  forall G A B,
        DerivI G (A ==>I A ∨I B)
       
| Ax9:  forall G A B,
        DerivI G (B ==>I A ∨I B)
       
| Ax10:  forall G A B C,
        DerivI G ((A ==>I C) ==>I (B ==>I C) ==>I A ∨I B ==>I C)
       
| MP:     forall G G' A B,
          DerivI G A -> DerivI G' (A ==>I B) -> DerivI (conc G G') B.
          
Global Hint Constructors DerivI : Intuitionistic.

Notation "G |-I A" := (DerivI G A) (at level 30).

(** Definition of equality between Formulas *)
Proposition eq_p_dec (A B: IFormula): {A = B} + {A <> B}.
Proof.
intros.
decide equality.
apply eq_nat_dec.
Qed.

Global Hint Resolve eq_p_dec : iformula.
